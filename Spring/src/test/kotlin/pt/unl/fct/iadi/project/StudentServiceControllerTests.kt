package pt.unl.fct.iadi.project

import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.SponsorControllerTests.Companion.mapper
import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.CVDAO
import pt.unl.fct.iadi.project.services.DAO.InstitutionDAO
import pt.unl.fct.iadi.project.services.DAO.NotFoundException
import pt.unl.fct.iadi.project.services.DAO.StudentDAO
import pt.unl.fct.iadi.project.services.DTOs.users.StudentDetailsDTO

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class StudentServiceControllerTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var students: StudentService

    companion object {

        val student1 = StudentDAO(1L, "rua do tramo", "pedru", "pedru","pedru@gmail", "24123421", "pass", Roles.ROLE_STUDENT, 31, "informatica", "mecanica mas tambem no roubo", InstitutionDAO(), mutableListOf(), null)
        val student2 = StudentDAO(2L, "rua do sad", "gdfg", "gdfg", "sdfggmail", "waes", "pafghss", Roles.ROLE_STUDENT, 31, "werwer", "werwer mas tambem no roubo", InstitutionDAO(), mutableListOf(), null)
        val studentDAO = listOf(student1, student2)

        val studentDTO = studentDAO.map { StudentDetailsDTO(it) }

        val CV1 = CVDAO(student1.id, mutableListOf())

        const val studentURL = "/students"

    }

    @Test
    fun `Test GET All students`() {

        Mockito.`when`(students.getAll()).thenReturn(studentDAO)

        mvc.perform(MockMvcRequestBuilders.get(studentURL))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    fun `Test GET One student`() {

        Mockito.`when`(students.getOne("pedru")).thenReturn(student1)

        val result = mvc.perform(MockMvcRequestBuilders.get("$studentURL/${student1.username}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<StudentDetailsDTO>(responseString)

        Assertions.assertEquals(responseDTO, studentDTO[0])
    }

    @Test
    fun `Test GET One student (Not Found)`() {

        Mockito.`when`(students.getOne("nonexistant"))
                .thenThrow(NotFoundException("Student with username nonexistant not found"))

        mvc.perform(MockMvcRequestBuilders.get("$studentURL/nonexistant"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)

    }

    @Test
    fun `Test DELETE One student`() {
        students.create(student1)
        students.create(student2)


        mvc.perform(MockMvcRequestBuilders.delete("$studentURL/${student1.username}"))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    fun `Test DELETE One student (not found)`() {


         Mockito.`when`(students.delete("nonexistant"))
                 .thenThrow(NotFoundException("Sponsor with username nonexistant not found"))

        mvc.perform(MockMvcRequestBuilders.delete("$studentURL/nonexistant"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)

    }

    @Test
    fun `Test getStudentApplications (emptyApplications)`() {

         Mockito.`when`(students.getStudentApplications("ola")).thenReturn(emptyList())

        mvc.perform(MockMvcRequestBuilders.get("$studentURL/ola/applications"))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    fun `Test getStudentApplications (one application)`() {

        Mockito.`when`(students.getStudentApplications("ola")).thenReturn(emptyList())

        mvc.perform(MockMvcRequestBuilders.get("$studentURL/ola/applications"))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    fun `Test getCVByStudentId (with CV)`() {


        Mockito.`when`(students.getCVByStudentId(student1.username)).thenReturn(CV1)

        mvc.perform(MockMvcRequestBuilders.get("$studentURL/${student1.username}/CV"))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    fun `Test getCVByStudentId (without CV)`() {

        Mockito.`when`(students.getCVByStudentId(student1.username))
                .thenThrow(NotFoundException("Student with email ${student1.username} does not have a CV"))

        mvc.perform(MockMvcRequestBuilders.get("$studentURL/${student1.username}/CV"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)


    }

}