package pt.unl.fct.iadi.project.grantApplication

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.hamcrest.Matchers.hasSize
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.GrantCallControllerTests
import pt.unl.fct.iadi.project.ReviewerControllerTests
import pt.unl.fct.iadi.project.SponsorControllerTests
import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.*
import java.util.*

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class GrantApplicationControllerTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var grantApplications: GrantApplicationService


    companion object {

        val CV = CVDAO(1L, mutableListOf())

        val student1 = StudentDAO(1L, "rua do tramo", "pedru", "pedru", "admin", "24123421", "pass", Roles.ROLE_STUDENT, 31, "informatica", "mecanica mas tambem no roubo", InstitutionDAO(), mutableListOf(), CV)
        val student2 = StudentDAO(1L, "rua do tramo", "pedru", "pedru", "pre", "24123421", "pass", Roles.ROLE_STUDENT, 31, "informatica", "mecanica mas tambem no roubo", InstitutionDAO(), mutableListOf(), CV)

        val grantApplication1 = GrantApplicationDAO(1L, Date(), 2, student1, GrantCallControllerTests.call1, mutableListOf(), null, mutableListOf())

        val applicationDAO = listOf(grantApplication1)
        val applicationDTO = applicationDAO.map { GrantApplicationDetailsDTO(it) }

        val finalEvaluationDAO = FinalEvaluationDAO(1L, "muito bom", 100f, ReviewerControllerTests.reviewer1, true)
        val finalEvaluationDTO = FinalEvaluationDTO(finalEvaluationDAO)

        val applicationsURL = "/grantApplications"

        val mapper = ObjectMapper().registerModule(KotlinModule())
    }

    @Test
    fun `Test Get One Application`() {
        val grantApplication = GrantApplicationDAO(1L, Date(), 2, student1, GrantCallControllerTests.call1, mutableListOf(), null, mutableListOf())
        val grantApplicationDTO = GrantApplicationDetailsDTO(grantApplication)

        Mockito.`when`(grantApplications.getOne(1)).thenReturn(grantApplication)

        val result = mvc.perform(MockMvcRequestBuilders.get("$applicationsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = SponsorControllerTests.mapper.readValue<GrantApplicationDetailsDTO>(responseString)

        Assertions.assertEquals(responseDTO, grantApplicationDTO)
    }


    @Test
    @WithMockUser(username="user", password = "pass", roles=["ADMIN"])
    fun `Test GET All Applications`() {

        Mockito.`when`(grantApplications.getAll()).thenReturn(applicationDAO)

        mvc.perform(MockMvcRequestBuilders.get(applicationsURL))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize<Any>(applicationDTO.size)))
    }


    @Test
    fun `Test Put (update) Application`() {
        val applicationDTO = GrantApplicationDTO(Date(), 2, listOf(), 3, "pre")
        val application = mapper.writeValueAsString(applicationDTO)

        Mockito.`when`(grantApplications.getOne(1)).thenReturn(applicationDAO[0])

        mvc.perform(MockMvcRequestBuilders.put("$applicationsURL/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(application))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `Test Delete Application`() {
        mvc.perform(MockMvcRequestBuilders.delete("$applicationsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `Test Get Final Evaluation`() {

        Mockito.`when`(grantApplications.getFinalEvaluationByApplicationId(1L)).thenReturn(finalEvaluationDAO)

        val result = mvc.perform(MockMvcRequestBuilders.get("/grantApplications/1/finalEvaluation"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = SponsorControllerTests.mapper.readValue<FinalEvaluationDTO>(responseString)

        // assertEquals porque nao da para comparar as variaveis DTO. Compara-se variavel a variavel
        // para verificar que estao todas com o mesmo valor
        Assertions.assertEquals(responseDTO.evaluation, finalEvaluationDTO.evaluation)
        Assertions.assertEquals(responseDTO.score, finalEvaluationDTO.score)
        Assertions.assertEquals(responseDTO.reviewerUsername, finalEvaluationDTO.reviewerUsername)
        Assertions.assertEquals(responseDTO.accepted, finalEvaluationDTO.accepted)

    }

}