package pt.unl.fct.iadi.project

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.users.SponsorDetailsDTO



@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class SponsorControllerTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var sponsors: SponsorService

    companion object {

        val google = SponsorDAO(1L, "Rua do carmo", "Google Inc", "google", "google@gmail.com", "+351 21939394", "gongas", Roles.ROLE_SPONSOR, emptyList())
        val microsoft = SponsorDAO(2L, "Rua do carmo", "Microsoft Inc", "microsoft","micro@hotmail.com", "+351 21939394", "micros", Roles.ROLE_SPONSOR, emptyList())
        val sponsorDAO = listOf(google, microsoft)

        val sponsorDTO = sponsorDAO.map { SponsorDetailsDTO(it) }

        const val sponsorURL = "/sponsors"
        const val callURL = "/grantCalls"

        val mapper = ObjectMapper().registerModule(KotlinModule())

    }

    @Test
    fun `Test GET sponsors`() {
        Mockito.`when`(sponsors.getAll()).thenReturn(sponsorDAO)

        mvc.perform(MockMvcRequestBuilders.get(sponsorURL))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize<Any>(sponsorDTO.size)))
    }

    @Test
    fun `Test GET One sponsor`() {

        Mockito.`when`(sponsors.getOne(google.username)).thenReturn(google)

        val result = mvc.perform(MockMvcRequestBuilders.get("$sponsorURL/${google.username}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<SponsorDetailsDTO>(responseString)

        Assertions.assertEquals(responseDTO, sponsorDTO[0])
    }

    @Test
    fun `Test GET One sponsor (Not Found)`() {

        Mockito.`when`(sponsors.getOne("nonexistant"))
                .thenThrow(NotFoundException("Sponsor with username nonexistant not found"))

        mvc.perform(MockMvcRequestBuilders.get("$sponsorURL/nonexistant"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)

    }


}