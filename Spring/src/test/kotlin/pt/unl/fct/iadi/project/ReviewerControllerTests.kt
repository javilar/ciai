package pt.unl.fct.iadi.project

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.users.ReviewerDTO
import pt.unl.fct.iadi.project.services.DTOs.users.ReviewerDetailsDTO

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class ReviewerControllerTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var reviewers: ReviewerService


    companion object {


        val reviewer1 = ReviewerDAO(1L, "rua do cramo", "pedro", "pedro","pedro@pedro", "asd", "pass", Roles.ROLE_REVIEWER, "curso", "5", InstitutionDAO(), emptyList(), emptyList())
        val reviewer2 = ReviewerDAO(2L, "rua do ze", "sds", "sds", "joao@joao", "sdsad", "pp", Roles.ROLE_REVIEWER, "curso", "9", InstitutionDAO(), emptyList(), emptyList())
        val reviewerDAO = listOf(reviewer1, reviewer2)

        val reviewerDTO = reviewerDAO.map { ReviewerDetailsDTO(it) }

        val mapper = ObjectMapper().registerModule(KotlinModule())

    }

    @Test
    fun `Test POST One reviewer`() {

        val reviewerDTO = ReviewerDTO("address", "Seixo Paulo", "seixo", "seixopapas@gimail.com", "191919", "pauloSeixo69", "mias", "200", 1)
        val reviewer = mapper.writeValueAsString(reviewerDTO)

        mvc.perform(MockMvcRequestBuilders.post("/reviewers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(reviewer))
                .andExpect(MockMvcResultMatchers.status().isCreated)

    }

    @Test
    fun `Test GET One reviewer`() {

        Mockito.`when`(reviewers.getOne(reviewer1.username)).thenReturn(reviewer1)

        val result = mvc.perform(MockMvcRequestBuilders.get("/reviewers/${reviewer1.username}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = SponsorControllerTests.mapper.readValue<ReviewerDetailsDTO>(responseString)

        Assertions.assertEquals(responseDTO, reviewerDTO[0])
    }

    @Test
    fun `Test GET One reviewer (Not Found)`() {

        Mockito.`when`(reviewers.getOne("notexists"))
                .thenThrow(NotFoundException("Reviewer with username notexists not found"))

        mvc.perform(MockMvcRequestBuilders.get("/reviewers/notexists"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)

    }


}