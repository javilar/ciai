package pt.unl.fct.iadi.project.grantApplication

import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.GrantCallControllerTests
import pt.unl.fct.iadi.project.TestData
import pt.unl.fct.iadi.project.SponsorControllerTests
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.*
import java.util.*

@SpringBootTest
@AutoConfigureMockMvc
class GrantApplicationSecurityTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var grantApplications: GrantApplicationService

    @MockBean
    lateinit var users: UserService

    companion object {

        private const val applicationsURL = "/grantApplications"

    }


    @Test
    @WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
    fun `Test Get One Application (Admin)`() {
        val grantApplication = GrantApplicationDAO(1L, Date(), 2, GrantApplicationControllerTests.student1, GrantCallControllerTests.call1, mutableListOf(), null, mutableListOf())
        val grantApplicationDTO = GrantApplicationDetailsDTO(grantApplication)

        Mockito.`when`(grantApplications.getOne(1)).thenReturn(grantApplication)

        val result = mvc.perform(MockMvcRequestBuilders.get("$applicationsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = SponsorControllerTests.mapper.readValue<GrantApplicationDetailsDTO>(responseString)

        Assertions.assertEquals(responseDTO, grantApplicationDTO)
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = ["STUDENT"])
    fun `Test Get One Application forbidden is not the student who created application`() {
        val grantApplication = GrantApplicationDAO(1L, Date(), 2, GrantApplicationControllerTests.student2, GrantCallControllerTests.call1, mutableListOf(), null, mutableListOf())

        Mockito.`when`(grantApplications.getOne(1)).thenReturn(grantApplication)
        Mockito.`when`(users.getOne("admin")).thenReturn(TestData.reviewer1)


        mvc.perform(MockMvcRequestBuilders.get("$applicationsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)

    }

    @Test
    @WithMockUser(username = "pre", password = "password", roles = ["REVIEWER"])
    fun `Test Get One Application forbidden is not the reviewer in the eval panel`() {
        val grantApplication = GrantApplicationDAO(1L, Date(), 2, GrantApplicationControllerTests.student2, GrantCallControllerTests.call1, mutableListOf(), null, mutableListOf())

        Mockito.`when`(grantApplications.getOne(1)).thenReturn(grantApplication)
        Mockito.`when`(users.getOne("admin")).thenReturn(TestData.reviewer1)


        mvc.perform(MockMvcRequestBuilders.get("$applicationsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)

    }

    @Test
    @WithMockUser(username = "pedro@pedro", password = "pass", roles = ["REVIEWER"])
    fun `Test Get One Application reviewer is in the eval panel`() {
        val grantApplication = GrantApplicationDAO(1L, Date(), 2, GrantApplicationControllerTests.student2, GrantCallControllerTests.call1, mutableListOf(), null, mutableListOf())

        Mockito.`when`(grantApplications.getOne(1)).thenReturn(grantApplication)
        Mockito.`when`(users.getOne("pedro@pedro")).thenReturn(TestData.reviewer1)


        mvc.perform(MockMvcRequestBuilders.get("$applicationsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    @WithMockUser(username = "pedro@pedro", password = "pass", roles = ["REVIEWER"])
    fun `Test Get All Applications forbidden not admin`() {

        mvc.perform(MockMvcRequestBuilders.get(applicationsURL))
                .andExpect(MockMvcResultMatchers.status().isForbidden)

    }


}