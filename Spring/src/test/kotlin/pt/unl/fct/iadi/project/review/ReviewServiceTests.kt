package pt.unl.fct.iadi.project.review

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import pt.unl.fct.iadi.project.TestData
import pt.unl.fct.iadi.project.model.GrantApplicationRepository
import pt.unl.fct.iadi.project.model.ReviewRepository
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.GrantApplicationService
import pt.unl.fct.iadi.project.services.ReviewService
import java.util.*


@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class ReviewServiceTests {

    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var applicationsService: GrantApplicationService

    @Autowired
    lateinit var reviewsService: ReviewService

    @MockBean
    lateinit var applicationsRepo: GrantApplicationRepository

    @MockBean
    lateinit var reviewsRepo: ReviewRepository

    @Test
    fun `Test Get Final Evaluation Not Found`() {
        Mockito.`when`(applicationsRepo.findById(1)).thenReturn(Optional.of(TestData.grantApplication1))

        Assertions.assertThrows(NotFoundException::class.java) { applicationsService.getFinalEvaluationByApplicationId(1) }
    }

    @Test
    fun `Test Get Application Reviews With No Final Evaluation`() {
        Mockito.`when`(applicationsRepo.findByIdWithReviews(1)).thenReturn(Optional.of(TestData.grantApplication1))

        Assertions.assertThrows(ApplicationDoesNotHaveFinalEvaluationException::class.java) { applicationsService.getApplicationReviews(1) }
    }

    @Test
    fun `Test Get Application Reviews`() {
        Mockito.`when`(applicationsRepo.findByIdWithReviews(3)).thenReturn(Optional.of(TestData.grantApplicationWithFinalEvaluation))

        Assertions.assertEquals(applicationsService.getApplicationReviews(3), mutableListOf(TestData.review1, TestData.review2))
    }

    @Test
    fun `Test Submit Review With No Evaluation Panel`() {
        Mockito.`when`(reviewsRepo.findById(1)).thenReturn(Optional.empty())

        Mockito.`when`(applicationsRepo.findById(1)).thenReturn(Optional.of(TestData.grantApplication2))

        Assertions.assertThrows(NotFoundException::class.java) { applicationsService.submitReview(1, TestData.review2) }
    }


    @Test
    fun `Test Submit Review With reviewer and student from same Institution`() {
        Mockito.`when`(reviewsRepo.findById(1)).thenReturn(Optional.empty())

        Mockito.`when`(applicationsRepo.findById(1)).thenReturn(Optional.of(TestData.grantApplication1))

        Assertions.assertThrows(ReviewerAndStudentFromSameInstitution::class.java) { applicationsService.submitReview(1, TestData.review1) }
    }

    @Test
    fun `Test Submit Review but reviewer is not in panel`() {

        Mockito.`when`(reviewsRepo.findById(1)).thenReturn(Optional.empty())

        Mockito.`when`(applicationsRepo.findById(1)).thenReturn(Optional.of(TestData.grantApplication3))

        Assertions.assertThrows(ReviewerIsNotInEvaluationPanel::class.java) { applicationsService.submitReview(1, TestData.review1) }
    }

    @Test
    fun `Test Get Review Not Found`() {
        Mockito.`when`(reviewsRepo.findById(1)).thenReturn(Optional.empty())

        Assertions.assertThrows(NotFoundException::class.java) { reviewsService.getOne(1) }
    }

}