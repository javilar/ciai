package pt.unl.fct.iadi.project.review

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.TestData
import pt.unl.fct.iadi.project.grantApplication.GrantApplicationControllerTests
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DTOs.*

@SpringBootTest
@AutoConfigureMockMvc
class ReviewSecurityTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var reviews: ReviewService

    @MockBean
    lateinit var reviewers: ReviewerService

    @MockBean
    lateinit var applications: GrantApplicationService

    companion object {

        private const val reviewsURL = "/reviews"

    }

    @Test
    @WithMockUser(username = "pedro@pedro", password = "password", roles = ["REVIEWER"])
    fun `Test Get One Review reviewer is the one who created the review`() {

        Mockito.`when`(reviews.getOne(1)).thenReturn(TestData.review1)

        mvc.perform(MockMvcRequestBuilders.get("$reviewsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "wrongUsername", password = "password", roles = ["REVIEWER"])
    fun `Test Get One Review forbidden reviewer is not the one who created the review`() {
        Mockito.`when`(reviews.getOne(1)).thenReturn(TestData.review1)

        mvc.perform(MockMvcRequestBuilders.get("$reviewsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }


    @Test
    @WithMockUser(username = "pedro@pedro", password = "password", roles = ["REVIEWER"])
    fun `Test Get Reviewer reviews authenticated user is the reviewer specified`() {
        Mockito.`when`(reviewers.getOne(TestData.reviewer1.email)).thenReturn(TestData.reviewer1)

        mvc.perform(MockMvcRequestBuilders.get("/reviewers/${TestData.reviewer1.email}/reviews"))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "wrongUsername", password = "password", roles = ["REVIEWER"])
    fun `Test Get Reviewer reviews forbidden authenticated user is not the reviewer specified`() {
        Mockito.`when`(reviewers.getOne(TestData.reviewer1.email)).thenReturn(TestData.reviewer1)

        mvc.perform(MockMvcRequestBuilders.get("/reviewers/${TestData.reviewer1.email}/reviews"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @Test
    @WithMockUser(username = "email.com", password = "password", roles = ["REVIEWER"])
    fun `Test Submit Review authenticated user is in the evaluation panel of the call`() {
        val reviewDTO = ReviewDTO(TestData.review1)
        val review = GrantApplicationControllerTests.mapper.writeValueAsString(reviewDTO)

        Mockito.`when`(reviewers.getOne(TestData.reviewer1.email)).thenReturn(TestData.reviewer1)
        Mockito.`when`(applications.getOne(1)).thenReturn(TestData.grantApplication1)

        mvc.perform(MockMvcRequestBuilders.post("/grantApplications/1/reviews")
                .contentType(MediaType.APPLICATION_JSON)
                .content(review))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "wrongUsername", password = "password", roles = ["REVIEWER"])
    fun `Test Submit Review forbidden authenticated user is not in the evaluation panel of the call`() {
        val reviewDTO = ReviewDTO(TestData.review1)
        val review = GrantApplicationControllerTests.mapper.writeValueAsString(reviewDTO)

        Mockito.`when`(applications.getOne(1)).thenReturn(TestData.grantApplication1)

        mvc.perform(MockMvcRequestBuilders.post("/grantApplications/1/reviews")
                .contentType(MediaType.APPLICATION_JSON)
                .content(review))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @Test
    @WithMockUser(username = "omil.com", password = "password", roles = ["REVIEWER"])
    fun `Test Submit Final evaluation authenticated user is the panel chair`() {
        val finalEvaluation = FinalEvaluationDTO(TestData.finalEvaluation2)
        val review = GrantApplicationControllerTests.mapper.writeValueAsString(finalEvaluation)

        Mockito.`when`(reviewers.getOne(TestData.reviewerToBeInPanel2.email)).thenReturn(TestData.reviewerToBeInPanel2)
        Mockito.`when`(applications.getOne(1)).thenReturn(TestData.grantApplication1)

        mvc.perform(MockMvcRequestBuilders.post("/grantApplications/1/finalEvaluation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(review))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(username = "wrongUsername", password = "password", roles = ["REVIEWER"])
    fun `Test Submit Final evaluation forbidden authenticated user is not the panel chair`() {
        val finalEvaluation = FinalEvaluationDTO(TestData.finalEvaluation1)
        val review = GrantApplicationControllerTests.mapper.writeValueAsString(finalEvaluation)

        Mockito.`when`(applications.getOne(1)).thenReturn(TestData.grantApplication1)

        mvc.perform(MockMvcRequestBuilders.post("/grantApplications/1/finalEvaluation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(review))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

}