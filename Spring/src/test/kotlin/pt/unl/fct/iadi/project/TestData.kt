package pt.unl.fct.iadi.project

import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.DAO.*
import java.util.*

object TestData {

    val sponsor1 = SponsorDAO(1L, "Rua do carmo", "Google Inc", "username1","google@gmail.com", "+351 21939394", "gongas", Roles.ROLE_SPONSOR, emptyList())
    val sponsor2 = SponsorDAO(2L, "Rua do carmo", "Microsoft Inc", "username2","micro@hotmail.com", "+351 21939394", "micros", Roles.ROLE_SPONSOR, emptyList())

    val reviewerToBeInPanel1 = ReviewerDAO(1L, "", "", "username3", "email.com", "", "", Roles.ROLE_REVIEWER, "", "", InstitutionDAO(), emptyList(), emptyList())
    val reviewerToBeInPanel2 = ReviewerDAO(1L, "", "", "username7","omil.com", "", "", Roles.ROLE_REVIEWER, "", "", InstitutionDAO(), emptyList(), emptyList())
    val evaluationPanel = EvaluationPanelDAO(1L, listOf(reviewerToBeInPanel1), reviewerToBeInPanel2)

    val call1 = GrantCallDAO(1L,"titulo", "pre", Date(), Date(), listOf("pre"),0.0, emptyList(), sponsor1, emptyList(), evaluationPanel)
    val call2 = GrantCallDAO(2L,"gdfgdfg", "asdsf", Date(), Date(), listOf("gsdfg"),0.0, emptyList(), sponsor2, emptyList(), null)
    val call3 = GrantCallDAO(3L,"aaaaaaa", "asdaasf", Date(), Date(), listOf("gsdfg"),0.0, emptyList(), sponsor2, emptyList(), null)

    val CV = CVDAO(1L, mutableListOf())

    val student1 = StudentDAO(1L, "rua do tramo", "pedru", "username8","pedru@gmail", "24123421", "pass", Roles.ROLE_STUDENT, 31, "informatica", "mecanica mas tambem no roubo", InstitutionDAO(), mutableListOf(), null)
    val student2 = StudentDAO(2L, "rua do sad", "gdfg", "username5","sdfggmail", "waes", "pafghss", Roles.ROLE_STUDENT, 31, "werwer", "werwer mas tambem no roubo", InstitutionDAO(), mutableListOf(), CV)
    val student3 = StudentDAO(2L, "rua do sad", "gdfg", "username4","sdfggmail", "waes", "pafghss", Roles.ROLE_STUDENT, 31, "werwer", "werwer mas tambem no roubo", InstitutionDAO(1L, "A escola"), mutableListOf(), CV)


    val grantApplication1 = GrantApplicationDAO(1L, Date(), 2, student1, call1, mutableListOf(), null, mutableListOf())
    val grantApplication2 = GrantApplicationDAO(2L, Date(), 2, student2, call2, mutableListOf(), null, mutableListOf())
    val grantApplication3 = GrantApplicationDAO(2L, Date(), 2, student3, call1, mutableListOf(), null, mutableListOf())

    val reviewer1 = ReviewerDAO(1L, "rua do cramo", "pedro", "pedro","pedro@pedro", "asd", "pass", Roles.ROLE_REVIEWER, "curso", "5", InstitutionDAO(), emptyList(), emptyList())

    val review1 = ReviewDAO(1L,"muito bom", 100f, reviewer1, grantApplication1)
    val review2 = ReviewDAO(2L,"muita mau", 10f)
    val reviewList = listOf(review1, review2)

    val finalEvaluation1 = FinalEvaluationDAO(1L, "muito bom", 100f, reviewer1, true)
    val finalEvaluation2 = FinalEvaluationDAO(1L, "muito bom", 100f, reviewerToBeInPanel2, true)

    val grantApplicationWithFinalEvaluation = GrantApplicationDAO(3L, Date(), 2, student2, call3, mutableListOf(), finalEvaluation1, mutableListOf(review1, review2))


}