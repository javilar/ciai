package pt.unl.fct.iadi.project.grantApplication

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import pt.unl.fct.iadi.project.TestData
import pt.unl.fct.iadi.project.model.GrantApplicationRepository
import pt.unl.fct.iadi.project.model.ReviewRepository
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.GrantApplicationService
import java.util.*


@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class GrantApplicationServiceTests {

    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var applicationsService: GrantApplicationService

    @MockBean
    lateinit var applicationsRepo: GrantApplicationRepository

    @MockBean
    lateinit var reviewsRepo: ReviewRepository

    @Test
    fun `Test Get All Applications`() {

        val applicationList = listOf<GrantApplicationDAO>(TestData.grantApplication1, TestData.grantApplication2)

        Mockito.`when`(applicationsRepo.findAll()).thenReturn(applicationList)

        Assertions.assertEquals(applicationsService.getAll(), applicationList)

    }

    @Test
    fun `Test Get One Application`() {

        Mockito.`when`(applicationsRepo.findById(1)).thenReturn(Optional.of(TestData.grantApplication1))

        Assertions.assertEquals(applicationsService.getOne(1), TestData.grantApplication1)

    }

    @Test
    fun `Test Post Application Already Exists`() {

        Mockito.`when`(applicationsRepo.findById(1))
                .thenReturn(Optional.of(TestData.grantApplication1))

        Assertions.assertThrows(AlreadyExistsException::class.java) { applicationsService.addOne(TestData.grantApplication1) }

    }

    @Test
    fun `Test Post Application On Call Not Open Yet`() {

        val calendar = Calendar.getInstance()
        calendar.set(2021, 11, 31, 59, 59, 59)
        val beforeDate: Date = calendar.time
        calendar.set(2022, 11, 31, 59, 59, 59)
        val afterDate: Date = calendar.time

        val notYetOpenCall = GrantCallDAO(3L,"aaaaaaa", "asdaasf", beforeDate, afterDate, listOf("gsdfg"),0.0, emptyList(), TestData.sponsor2, emptyList(), null)
        val grantApplication = GrantApplicationDAO(2L, Date(), 2, TestData.student2, notYetOpenCall, mutableListOf(), null, mutableListOf())


        Assertions.assertThrows(GrantCallNotOpenYetException::class.java) { applicationsService.addOne(grantApplication) }

    }

    @Test
    fun `Test Post Application On Call Already Closed`() {

        val calendar = Calendar.getInstance()
        calendar.set(2018, 11, 31, 59, 59, 59)
        val beforeDate: Date = calendar.time
        calendar.set(2019, 11, 31, 59, 59, 59)
        val afterDate: Date = calendar.time

        val alreadyClosedCall = GrantCallDAO(3L,"aaaaaaa", "asdaasf", beforeDate, afterDate, listOf("gsdfg"),0.0, emptyList(), TestData.sponsor2, emptyList(), null)
        val grantApplication = GrantApplicationDAO(2L, Date(), 2, TestData.student2, alreadyClosedCall, mutableListOf(), null, mutableListOf())


        Assertions.assertThrows(GrantCallAlreadyClosedException::class.java) { applicationsService.addOne(grantApplication) }

    }

    @Test
    fun `Test Post Application With Student without CV`() {

        val calendar = Calendar.getInstance()
        calendar.set(2018, 11, 31, 59, 59, 59)
        val beforeDate: Date = calendar.time
        calendar.set(2021, 11, 31, 59, 59, 59)
        val afterDate: Date = calendar.time

        val alreadyClosedCall = GrantCallDAO(3L,"aaaaaaa", "asdaasf", beforeDate, afterDate, listOf("gsdfg"),0.0, emptyList(), TestData.sponsor2, emptyList(), null)
        val grantApplication = GrantApplicationDAO(2L, Date(), 2, TestData.student1, alreadyClosedCall, mutableListOf(), null, mutableListOf())


        Assertions.assertThrows(StudentDoesNotHaveCVException::class.java) { applicationsService.addOne(grantApplication) }

    }



}