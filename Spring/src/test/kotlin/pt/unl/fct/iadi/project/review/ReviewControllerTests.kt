package pt.unl.fct.iadi.project.review

import com.fasterxml.jackson.module.kotlin.readValue
import org.hamcrest.Matchers
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.GrantCallControllerTests
import pt.unl.fct.iadi.project.TestData
import pt.unl.fct.iadi.project.SponsorControllerTests
import pt.unl.fct.iadi.project.grantApplication.GrantApplicationControllerTests
import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.ReviewDTO
import pt.unl.fct.iadi.project.services.GrantApplicationService
import pt.unl.fct.iadi.project.services.ReviewService
import pt.unl.fct.iadi.project.services.ReviewerService

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class ReviewControllerTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var reviews: ReviewService

    @MockBean
    lateinit var reviewers: ReviewerService

    @MockBean
    lateinit var grantApplications: GrantApplicationService

    companion object {
        val review1 = ReviewDAO(1L,"muito bom", 100f)

        const val reviewsURL = "/reviews"
    }

    @Test
    fun `Test Get One Review`() {
        val reviewDTO = ReviewDTO(review1)

        Mockito.`when`(reviews.getOne(1)).thenReturn(review1)

        val result = mvc.perform(MockMvcRequestBuilders.get("$reviewsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = SponsorControllerTests.mapper.readValue<ReviewDTO>(responseString)

        Assertions.assertEquals(responseDTO.reviewerUsername, reviewDTO.reviewerUsername)
        Assertions.assertEquals(responseDTO.evaluation, reviewDTO.evaluation)
        Assertions.assertEquals(responseDTO.score, reviewDTO.score)
    }

    @Test
    fun `Test Get One review Not found`() {
        Mockito.`when`(reviews.getOne(1)).thenThrow(NotFoundException("Review with id 1 does not exist"))

        mvc.perform(MockMvcRequestBuilders.get("$reviewsURL/1"))
                .andExpect(MockMvcResultMatchers.status().isNotFound)
    }


    @Test
    fun `Test GET application reviews`() {
        Mockito.`when`(grantApplications.getApplicationReviews(1)).thenReturn(TestData.reviewList)

        mvc.perform(MockMvcRequestBuilders.get("${GrantApplicationControllerTests.applicationsURL}/1/reviews"))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `Test GET application reviews (PreCondition Failed) Does not have Final Evaluation`() {
        Mockito.`when`(grantApplications.getApplicationReviews(1)).thenThrow(
                ApplicationDoesNotHaveFinalEvaluationException("application with id 1 " +
                        "does not have a decision from its evaluation panel. You cannot check the reviews yet - " +
                        "please wait for the decision of the panel."))

        mvc.perform(MockMvcRequestBuilders.get("${GrantApplicationControllerTests.applicationsURL}/1/reviews"))
                .andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
    }

    @Test
    fun `Test GET all reviewer reviews`() {
        val reviewer1 = ReviewerDAO(1L, "rua do cramo", "pedro", "pedro",
                "pedro@pedro", "asd", "pass", Roles.ROLE_REVIEWER, "curso",
                "5", InstitutionDAO(), listOf(review1), listOf(GrantCallControllerTests.evaluationPanelDAO))

        Mockito.`when`(reviewers.getReviewerReviews(reviewer1.username)).thenReturn(listOf(review1))

        mvc.perform(MockMvcRequestBuilders.get("/reviewers/${reviewer1.username}/reviews"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$",
                        Matchers.hasSize<Any>(listOf(review1).size)))

    }

}