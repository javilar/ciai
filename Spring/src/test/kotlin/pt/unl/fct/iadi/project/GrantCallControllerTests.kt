package pt.unl.fct.iadi.project

import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.EvaluationPanelDetailsDTO
import pt.unl.fct.iadi.project.services.DTOs.GrantCallDetailsDTO
import java.util.*

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "password", roles = ["ADMIN"])
class GrantCallControllerTests {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var calls: GrantCallService


    companion object {

        val evaluationPanelDAO = EvaluationPanelDAO(1L, ReviewerControllerTests.reviewerDAO, ReviewerControllerTests.reviewer1)
        val evaluationPanelDTO = EvaluationPanelDetailsDTO(evaluationPanelDAO)


        val call1 = GrantCallDAO(1L,"titulo", "pre", Date(),Date(), listOf("pre"),0.0, emptyList(), SponsorControllerTests.google, emptyList(), evaluationPanelDAO)
        val call2 = GrantCallDAO(2L,"gdfgdfg", "asdsf", Date(),Date(), listOf("gsdfg"),0.0, emptyList(), SponsorControllerTests.microsoft, emptyList(), null)
        val callDAO = listOf(call1,call2)


        val callDTO = callDAO.map { (GrantCallDetailsDTO(it))}
    }

    @Test
    fun `Test GET One call`() {

        Mockito.`when`(calls.getOne(1)).thenReturn(call1)

        val result = mvc.perform(MockMvcRequestBuilders.get("${SponsorControllerTests.callURL}/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = SponsorControllerTests.mapper.readValue<GrantCallDetailsDTO>(responseString)

        Assertions.assertEquals(responseDTO, callDTO[0])
    }


    @Test
    fun `Test Get evaluation panel of Call`(){
        Mockito.`when`(calls.getEvaluationPanel(1)).thenReturn(evaluationPanelDAO)

        val result = mvc.perform(MockMvcRequestBuilders.get("/grantCalls/1/evaluationPanel"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = SponsorControllerTests.mapper.readValue<EvaluationPanelDetailsDTO>(responseString)

        Assertions.assertEquals(responseDTO, evaluationPanelDTO)
    }


}