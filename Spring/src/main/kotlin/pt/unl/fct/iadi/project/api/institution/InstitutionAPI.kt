package pt.unl.fct.iadi.project.api.institution

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.services.DTOs.InstitutionDTO

@Api(value = "Institutions", description = "The Institutions API", tags = ["Institutions"])
@RequestMapping("/institutions")
interface InstitutionAPI {

    @ApiOperation(value = "Get the list of Institutions in the system", nickname = "getAllInstitutions")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list of Institutions"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("", produces = ["application/json"])
    fun getAll(): List<InstitutionDTO>


    @ApiOperation(value = "Get the Institution with ID", nickname = "getOneInstitution")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the Institution with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}", produces = ["application/json"])
    fun getOne(@PathVariable id: Long): InstitutionDTO
}