package pt.unl.fct.iadi.project.services

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.SponsorRepository
import pt.unl.fct.iadi.project.model.StudentRepository
import pt.unl.fct.iadi.project.model.UserRepository
import pt.unl.fct.iadi.project.services.DAO.*

@Service
class SponsorService(val sponsors: SponsorRepository, val users: UserRepository) {

    fun getAll(): Iterable<SponsorDAO> = sponsors.findAll()

    fun getOne(username: String): SponsorDAO =  sponsors.findById(username).orElseThrow {
        NotFoundException("Sponsor with username $username not found")
    }

    fun getSponsorCalls(username: String): List<GrantCallDAO> {
        val sponsor = sponsors.findByIdWithCalls(username).orElseThrow {
            NotFoundException("Sponsor with username $username not found")
        }
        return sponsor.calls
    }

    fun create(sponsor: SponsorDAO){
        val sponsorUsername: String = sponsor.username
        users.findById(sponsorUsername).ifPresent {
            throw AlreadyExistsException("A user with username $sponsorUsername already exists")
        }
        sponsor.password =  BCryptPasswordEncoder().encode(sponsor.password)
        sponsors.save(sponsor)
    }

}