package pt.unl.fct.iadi.project.services.DAO

import java.util.*
import javax.persistence.*

@Entity
data class GrantCallDAO(
        @Id
        @GeneratedValue
        var id:Long = 0,
        var title: String = "",
        var description: String = "",

        @Temporal(TemporalType.DATE)
        var openingDate: Date = Date(),

        @Temporal(TemporalType.DATE)
        var closingDate: Date = Date(),

        @ElementCollection
        var requirements: List<String> = emptyList(),

        var funding: Double = 0.0,

        @OneToMany(cascade = [CascadeType.ALL])
        var dataItems : List<QuestionFieldDAO> = emptyList(),

        @ManyToOne
        var sponsor: SponsorDAO = SponsorDAO(),

        @OneToMany(mappedBy = "grantCall", cascade = [CascadeType.ALL])
        var applications: List<GrantApplicationDAO> = emptyList(),

        @OneToOne(mappedBy = "call", cascade = [CascadeType.ALL])
        var evaluationPanel: EvaluationPanelDAO? = null
)
{

        fun getQuestionByID(questionID: Long): QuestionFieldDAO? {
            return dataItems.find { it.id == questionID }
        }

        fun hasEvaluationPanel() = (evaluationPanel != null)

}