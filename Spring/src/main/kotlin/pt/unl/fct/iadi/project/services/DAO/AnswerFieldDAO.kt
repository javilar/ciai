package pt.unl.fct.iadi.project.services.DAO

import javax.persistence.*


@Entity
data class AnswerFieldDAO(
        @Id
        @GeneratedValue
        var id: Long = 0,
        var dataItem: String = "",

        @ManyToOne
        var question: QuestionFieldDAO = QuestionFieldDAO()
)