package pt.unl.fct.iadi.project.config

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.services.UserService


@Service
class CustomUserDetailsService(
        val users: UserService
) : UserDetailsService
{
    override fun loadUserByUsername(username: String?): UserDetails {

        username?.let {

            val userDAO = users.getOne(username)

            val authoritiesList = mutableListOf<GrantedAuthority>()

            authoritiesList.add(SimpleGrantedAuthority(userDAO.role.name))

            return CustomUserDetails(userDAO.username, userDAO.password, authoritiesList)



        }

        throw UsernameNotFoundException(username)

    }

}

class CustomUserDetails(private val username: String,
                        private val password: String,
                        private val authoritiesList: MutableCollection<out GrantedAuthority>) : UserDetails {



    override fun getAuthorities(): MutableCollection<out GrantedAuthority>
        = authoritiesList

    override fun getPassword(): String = password

    override fun getUsername(): String = username

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = true


}


