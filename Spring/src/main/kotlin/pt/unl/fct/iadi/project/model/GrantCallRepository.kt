package pt.unl.fct.iadi.project.model

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.GrantCallDAO
import java.util.*

interface GrantCallRepository: CrudRepository<GrantCallDAO, Long> {

    @Query("select c from GrantCallDAO c inner join fetch c.applications where c.id = :id")
    fun findByIdWithApplications(id: Long): Optional<GrantCallDAO>

    @Query("select c from GrantCallDAO c inner join fetch c.evaluationPanel where c.id = :id")
    fun findByIdWithEvaluationPanel(id: Long): Optional<GrantCallDAO>

    fun findAllByOpeningDateBeforeAndClosingDateAfter(openingDate: Date, closingDate: Date): Iterable<GrantCallDAO>

}