package pt.unl.fct.iadi.project.services.DAO

import org.springframework.lang.NonNull
import java.util.*
import javax.persistence.*

@Entity
data class GrantApplicationDAO(
        @Id
        @GeneratedValue
        val id: Long = 0,
        @Temporal(TemporalType.DATE)
        var submissionDate: Date = Date(),
        var status: Int = 0,

        @ManyToOne
        var student: StudentDAO = StudentDAO(),

        @ManyToOne
        @NonNull
        var grantCall: GrantCallDAO = GrantCallDAO(),

        @OneToMany(cascade = [CascadeType.ALL])
        var answers: MutableList<AnswerFieldDAO> = mutableListOf(),

        @OneToOne(mappedBy = "application", cascade = [CascadeType.ALL])
        var finalEvaluation: FinalEvaluationDAO? = null,

        @OneToMany(mappedBy = "application" ,cascade = [CascadeType.ALL])
        var reviews: MutableList<ReviewDAO> = mutableListOf()
)

{

        fun hasFinalEvaluation() = (finalEvaluation != null)

}