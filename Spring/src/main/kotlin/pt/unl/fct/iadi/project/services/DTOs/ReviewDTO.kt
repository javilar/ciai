package pt.unl.fct.iadi.project.services.DTOs

import pt.unl.fct.iadi.project.services.DAO.FinalEvaluationDAO
import pt.unl.fct.iadi.project.services.DAO.GrantApplicationDAO
import pt.unl.fct.iadi.project.services.DAO.ReviewDAO
import pt.unl.fct.iadi.project.services.DAO.ReviewerDAO

data class ReviewDTO(val evaluation:String,
                     val score: Float,
                     val reviewerUsername: String
) {
    constructor(it: ReviewDAO) : this(it.evaluation, it.score, it.reviewer.username)

    fun toDAO(reviewer: ReviewerDAO, application: GrantApplicationDAO): ReviewDAO =
            ReviewDAO(0, evaluation, score, reviewer, application)
}

class FinalEvaluationDTO(val evaluation:String,
                         val score: Float,
                         val reviewerUsername: String,
                         val accepted: Boolean

)
    //: ReviewDTO(evaluation, score, reviewerEmail)
    {

    constructor(it: FinalEvaluationDAO) : this (it.evaluation, it.score, it.reviewer.username, it.accepted)

    fun toDAO(reviewer: ReviewerDAO, application: GrantApplicationDAO): FinalEvaluationDAO =
            FinalEvaluationDAO(0, evaluation, score, reviewer, accepted, application)
}


