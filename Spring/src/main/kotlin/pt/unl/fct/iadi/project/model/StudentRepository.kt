package pt.unl.fct.iadi.project.model

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.StudentDAO
import java.util.*

interface StudentRepository: CrudRepository<StudentDAO, String> {

    @Query("select s from StudentDAO s inner join fetch s.applications where s.username = :username")
    fun findByIdWithApplications(username: String): Optional<StudentDAO>

    @Query("select s from StudentDAO s inner join fetch s.CV cv inner join fetch cv.answers where s.username = :username")
    fun findByIdWithCVAnswers(username: String): Optional<StudentDAO>
}