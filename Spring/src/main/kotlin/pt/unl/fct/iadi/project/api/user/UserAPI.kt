package pt.unl.fct.iadi.project.api.user

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.services.DTOs.users.UserDetailsDTO

/**
 * Interface for the User Controller Class.
 */

@Api(value = "Users", description = "The Users API", tags = ["Users"])
@RequestMapping("/users")
interface UserAPI {

    @ApiOperation(value = "Get the list of all users (Student/Sponsor/Reviewer) currently in the system", nickname = "getAll")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved all Users currently in the system"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("")
    fun getAll(): List<UserDetailsDTO>

    @ApiOperation(value = "Get the User (Student/Sponsor/Reviewer) with given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the User with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}")
    fun getOne(@PathVariable username: String): UserDetailsDTO

}