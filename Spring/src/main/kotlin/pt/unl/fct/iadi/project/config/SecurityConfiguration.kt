package pt.unl.fct.iadi.project.config

import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import pt.unl.fct.iadi.project.services.UserService

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfiguration(val users: UserService,
                            val customUserDetailsService: CustomUserDetailsService) : WebSecurityConfigurerAdapter() {

    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

    override fun configure(auth: AuthenticationManagerBuilder) {

        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(passwordEncoder().encode("password"))
                .roles("ADMIN")
                .and()
                .passwordEncoder(passwordEncoder())
                .and()
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder())

    }

    override  fun configure(http: HttpSecurity){
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/v2/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers(HttpMethod.POST ,"/sponsors").permitAll()
                .antMatchers(HttpMethod.POST ,"/students").permitAll()
                .antMatchers(HttpMethod.POST ,"/reviewers").permitAll()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers(HttpMethod.GET, "/grantCalls").permitAll()
                .antMatchers(HttpMethod.GET, "/grantCalls/*").permitAll()
                .antMatchers(HttpMethod.GET, "/grantCalls/*/acceptedApplications").permitAll()
                .antMatchers(HttpMethod.GET, "/grantApplications/*").permitAll()
                .antMatchers(HttpMethod.GET, "/grantApplications/*/finalEvaluation").permitAll()
                .antMatchers(HttpMethod.GET, "/grantApplications/*/reviews").permitAll()
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(UserPasswordAuthenticationFilterToJWT ("/login", super.authenticationManagerBean()),
                        BasicAuthenticationFilter::class.java)
                .addFilterBefore(UserPasswordSignUpFilterToJWT ("/signup", users),
                        BasicAuthenticationFilter::class.java)
                .addFilterBefore(JWTAuthenticationFilter(users),
                        BasicAuthenticationFilter::class.java)

    }


}