package pt.unl.fct.iadi.project.services.DTOs

import pt.unl.fct.iadi.project.services.DAO.CVDAO
import pt.unl.fct.iadi.project.services.DAO.QuestionFieldDAO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.AnswerFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.AnswerFieldDetailsDTO



data class CVDTO(val answerFields: List<AnswerFieldDTO>)
{
    constructor(it: CVDAO) : this (it.answers.map { AnswerFieldDTO(it) })

    fun toDAO(questions: List<QuestionFieldDAO>): CVDAO {

        return CVDAO(0, answerFields.map { answer ->
            answer.toDAO(questions.find { it.id == answer.questionID }!!)
        }.toMutableList())

    }

}

data class CVDetailsDTO(val id:Long, val answerFields: List<AnswerFieldDetailsDTO>)
{
    constructor(it: CVDAO) : this (it.id, it.answers.map { AnswerFieldDetailsDTO(it) })

}