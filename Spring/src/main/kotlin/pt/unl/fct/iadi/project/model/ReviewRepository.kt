package pt.unl.fct.iadi.project.model

import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.ReviewDAO

interface ReviewRepository: CrudRepository<ReviewDAO, Long> {
}