package pt.unl.fct.iadi.project.services.DTOs.users

import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.DAO.UserDAO

/**
 * Open class is an abstract class.
 * Abstract class to represent the Users in the system, and related child classes.
 */

open class UserDTO(
        open val address: String,
        open val name: String,
        open val username: String,
        open val email: String,
        open val phoneNumber: String,
        open val password: String,
        val role: Roles)

open class UserDetailsDTO(
        open val id: Long,
        open val address: String,
        open val name: String,
        open val username: String,
        open val email: String,
        open val phoneNumber: String,
        val role: Roles)
{
    constructor(it: UserDAO): this(it.id, it.address, it.name, it.username, it.email, it.phoneNumber, it.role)
}
