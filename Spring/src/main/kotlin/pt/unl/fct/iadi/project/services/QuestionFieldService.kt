package pt.unl.fct.iadi.project.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.QuestionFieldRepository
import pt.unl.fct.iadi.project.services.DAO.NotFoundException
import pt.unl.fct.iadi.project.services.DAO.QuestionFieldDAO

@Service
class QuestionFieldService(val questions: QuestionFieldRepository) {

    fun getAll(): Iterable<QuestionFieldDAO> = questions.findAll()

    fun getOne(id: Long): QuestionFieldDAO = questions.findById(id).orElseThrow {
        NotFoundException("Questionfield with id $id does not exist")
    }

    fun addOne(question: QuestionFieldDAO){
        questions.save(question)
    }

}