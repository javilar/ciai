package pt.unl.fct.iadi.project.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.UserRepository
import pt.unl.fct.iadi.project.services.DAO.AlreadyExistsException
import pt.unl.fct.iadi.project.services.DAO.NotFoundException
import pt.unl.fct.iadi.project.services.DAO.UserDAO

@Service
class UserService(val users: UserRepository) {

    fun getAll(): Iterable<UserDAO> = users.findAll()

    fun getOne(username: String): UserDAO = users.findById(username).orElseThrow {
        NotFoundException("User with username $username not found")
    }

    fun addOne(user: UserDAO) {
        users.findById(user.username).ifPresent{
            throw AlreadyExistsException()
        }

        users.save(user)
    }

}