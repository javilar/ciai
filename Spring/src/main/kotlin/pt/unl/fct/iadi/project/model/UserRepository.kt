package pt.unl.fct.iadi.project.model

import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.UserDAO

interface UserRepository: CrudRepository<UserDAO, String> {


}