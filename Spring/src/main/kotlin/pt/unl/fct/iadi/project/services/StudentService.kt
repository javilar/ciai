package pt.unl.fct.iadi.project.services

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.StudentRepository
import pt.unl.fct.iadi.project.model.UserRepository
import pt.unl.fct.iadi.project.services.DAO.*

@Service
class StudentService(val students: StudentRepository, val users: UserRepository) {

    fun getAll(): Iterable<StudentDAO> = students.findAll()

    fun getOne(username: String): StudentDAO =  students.findById(username).orElseThrow {
        NotFoundException("Student with username $username not found")
    }

    fun create(student: StudentDAO){
        val studentUsername = student.username
        users.findById(studentUsername).ifPresent {
            throw AlreadyExistsException("A user with username $studentUsername already exists")
        }
        student.password =  BCryptPasswordEncoder().encode(student.password)
        students.save(student)
    }

    fun update(student: StudentDAO){
        val studentUsername = student.username

        students.findById(studentUsername).orElseThrow {
            NotFoundException("Student with username $studentUsername does not exist")
        }

        students.save(student)
    }

    fun delete(username: String) {
        val student: StudentDAO = getOne(username)

        return students.delete(student)
    }

    fun getStudentApplications(username: String): List<GrantApplicationDAO> {
        val student: StudentDAO = students.findByIdWithApplications(username).orElseThrow {
            NotFoundException("Student with username $username does not exist")
        }

        return student.applications
    }

    fun createCV(username: String, CV: CVDAO) {
        val student = getOne(username)

        if(student.CV != null)
            throw AlreadyExistsException("Student with username $username already has a CV")

        student.CV = CV
        students.save(student)
    }

    fun getCVByStudentId(username: String) : CVDAO {
        val student = getOne(username)

        if(student.CV == null)
            throw NotFoundException("Student with username $username does not have a CV")

        return student.CV!!
    }

    fun updateCV(username: String, CVFields: List<AnswerFieldDAO>) {
        val student = students.findByIdWithCVAnswers(username).orElseThrow {
            NotFoundException("Student with username $username not found")
        }

        if(student.CV == null)
            throw NotFoundException("Student with username $username does not have a CV")

        student.CV!!.answers = CVFields.toMutableList()
        students.save(student)
    }
}