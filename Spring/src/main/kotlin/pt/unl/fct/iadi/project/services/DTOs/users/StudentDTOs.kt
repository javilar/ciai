package pt.unl.fct.iadi.project.services.DTOs.users

import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.DAO.InstitutionDAO
import pt.unl.fct.iadi.project.services.DAO.StudentDAO
import pt.unl.fct.iadi.project.services.DTOs.InstitutionDTO

data class StudentDTO(
        override val address: String,
        override val name: String,
        override val username: String,
        override val email: String,
        override val phoneNumber: String,
        override val password: String,
        val number: Int,
        val course: String,
        val degree: String,
        val institutionId: Long) : UserDTO(address, name, username, email, phoneNumber, password, Roles.ROLE_STUDENT)
{
    constructor(it: StudentDAO): this(it.address, it.name, it.username, it.email,
            it.phoneNumber, it.password, it.number, it.course, it.degree, it.institution.id)

    fun toDAO(institution: InstitutionDAO): StudentDAO =
            StudentDAO(0, address, name, username, email, phoneNumber, password,
                    Roles.ROLE_STUDENT, number, course, degree, institution)

    fun updateDAO(studentToUpdate: StudentDAO): StudentDAO {
        studentToUpdate.address = this.address
        studentToUpdate.name = this.name
        studentToUpdate.username = this.username
        studentToUpdate.email = this.email
        studentToUpdate.phoneNumber = this.phoneNumber
        studentToUpdate.password = this.password
        studentToUpdate.number = this.number
        studentToUpdate.course = this.course
        studentToUpdate.degree = this.degree

        return studentToUpdate
    }

}

data class StudentDetailsDTO(
        override val id: Long,
        override val address:String,
        override val name: String,
        override val username: String,
        override val email: String,
        override val phoneNumber: String,
        val institution: InstitutionDTO,
        val number: Int,
        val course: String,
        val degree: String) : UserDetailsDTO(id, address, name, username, email, phoneNumber, Roles.ROLE_STUDENT)
{
    constructor(it: StudentDAO): this(it.id, it.address, it.name, it.username, it.email, it.phoneNumber,
            InstitutionDTO(it.institution), it.number, it.course, it.degree)
}