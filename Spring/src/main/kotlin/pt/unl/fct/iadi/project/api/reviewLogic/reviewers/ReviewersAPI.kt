package pt.unl.fct.iadi.project.api.reviewLogic.reviewers

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.security.SecurityAnnotation
import pt.unl.fct.iadi.project.services.DTOs.GrantApplicationDetailsDTO
import pt.unl.fct.iadi.project.services.DTOs.ReviewDTO
import pt.unl.fct.iadi.project.services.DTOs.users.ReviewerDTO
import pt.unl.fct.iadi.project.services.DTOs.users.ReviewerDetailsDTO

@Api(value = "Reviewers", description = "The Reviewers API", tags = ["Reviewers"])
@RequestMapping("/reviewers")
interface ReviewersAPI {

    @ApiOperation(value = "Get the reviewer with a given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the reviewer with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}")
    fun getOne(@PathVariable username: String): ReviewerDetailsDTO

    @ApiOperation(value = "Create a new Reviewer in the system")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully created a new Reviewer in the system"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    fun create(@RequestBody reviewer: ReviewerDTO)


    @ApiOperation(value = "Get the reviews of the reviewer with given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the reviewer with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}/reviews")
    @SecurityAnnotation.IsAllowedViewReviewersReviews
    fun getReviews(@PathVariable username: String): List<ReviewDTO>


    @ApiOperation(value = "Get all the applications assigned to panels the reviewer with given ID belongs to")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the applications assigned to panels the reviewer with given ID belongs to"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}/applications")
    @SecurityAnnotation.IsAllowedViewReviewersReviews
    fun getReviewerApplications(@PathVariable username: String): List<GrantApplicationDetailsDTO>


    @ApiOperation(value = "Get all the applications assigned to panels the reviewer with given ID leads")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the applications assigned to panels the reviewer with given ID leads"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}/leadApplications")
    @SecurityAnnotation.IsAllowedViewReviewersReviews
    fun getPanelChairApplications(@PathVariable username: String): List<GrantApplicationDetailsDTO>

}