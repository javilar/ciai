package pt.unl.fct.iadi.project.config

import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import pt.unl.fct.iadi.project.model.*
import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.GrantCallDTO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.AnswerFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.QuestionFieldDTO
import java.util.*

@Component
class SeedData(var institutionRepo: InstitutionRepository,
               var questions: QuestionFieldService,
               var calls: GrantCallRepository,
               var applications: GrantApplicationRepository,
               var sponsors: SponsorRepository,
               var reviewers: ReviewerRepository,
               var students: StudentRepository,
               var evalPanels: EvaluationPanelRepository,
               var reviews: ReviewRepository,
               var evaluations: FinalEvaluationRepository,
               var callService: GrantCallService
               ){

    @EventListener
    fun appReady(event: ApplicationReadyEvent) {


        //Save some institutions into the memory db
        institutionRepo.save(InstitutionDAO(1, "FCT", "919191919"))
        institutionRepo.save(InstitutionDAO(2, "IST", "888888888"))
        institutionRepo.save(InstitutionDAO(3, "Miksoft", "834567381"))

        //Save some CV questions into the memory db
        questions.addOne(QuestionFieldDAO(4, "Achas que sabes mais que um miudo de 12 anos?",
                "Tens que saber...", true, "Text"))
        questions.addOne(QuestionFieldDAO(5, "Diploma de mestrado",
                "Insere o teu diploma de mestre", true, "Attachment"))
        questions.addOne(QuestionFieldDAO(6, "Diploma de inglês - nível avançado (C1)",
                "Insere o teu diploma nivel avançado em Inglês", true, "Attachment"))
        questions.addOne(QuestionFieldDAO(7, "Pescas alguma coisa de algo?",
                "Arroz de cabidela", false, "Text"))


        // Save a sponsor
        val sponsor1 = SponsorDAO(0, "Rua Boa", "MikeSponsor", "mikesponsor", "mikesponsor@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_SPONSOR, mutableListOf())
        val sponsor2 = SponsorDAO(0, "Avenida Txi", "JohnSponsor", "johnsponsor", "johnsponsor@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_SPONSOR, mutableListOf())
        val sponsor3 = SponsorDAO(0, "Largo do Malaquias", "TerrySponsor", "terrysponsor", "terrysponsor@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_SPONSOR, mutableListOf())

        sponsors.save(sponsor1);
        sponsors.save(sponsor2);
        sponsors.save(sponsor3);



        var question1 = QuestionFieldDTO("Leite antes ou depois dos cereais?", "Pensa bem.", true, "Text")
        var question2 = QuestionFieldDTO("Diploma de inglês", "IELTS, TOEFL, Cambridge ou Farinha Amparo", false, "File")
        var question3 = QuestionFieldDTO("Sabes mais que um miudo de 12 anos?", "Tens que saber", false, "Choice")

        var questionList1 = listOf<QuestionFieldDTO>(question1, question2, question3).map { it.toDAO() }

        var question4 = QuestionFieldDTO("Diz ai a tua media", "Pensa bem.", true, "Text")
        var question5 = QuestionFieldDTO("Diploma de inglês", "IELTS, TOEFL, Cambridge ou Farinha Amparo", false, "File")
        var question6 = QuestionFieldDTO("Sabes muito disto?", "Tens que saber", true, "Choice")

        var questionList2 = listOf<QuestionFieldDTO>(question4, question5, question6).map { it.toDAO() }

        var question7 = QuestionFieldDTO("Leite antes ou depois dos cereais?", "Pensa bem.", true, "Text")
        var question8 = QuestionFieldDTO("Diploma de lorem ipsum", "Lorem 20 no trabalho por favor ipsum", false, "File")
        var question9 = QuestionFieldDTO("Fizeste curso de informatica?", "Sim ou nao chefe", true, "Choice")

        var questionList3 = listOf<QuestionFieldDTO>(question7, question8, question9).map { it.toDAO() }



        var dateFuture = Date()
        dateFuture.setTime(1639267199000)

        var datePast1 = Date()
        datePast1.setTime(1601856000000)

        var datePast2 = Date()
        datePast2.setTime(1604620799000)

        //val call1 = GrantCallDAO(0, "MikeSponsor Grant Call!!", "Isto é definitivamente uma grant call. Terás de fazer montes de coisas e nem vais ser pago!", datePast1, dateFuture, mutableListOf("Isto é um requirement", "E isto é outro"), 200.0, questionList.map { it.toDAO() }, sponsor1, mutableListOf())
        //val call2 = GrantCallDAO(0, "JohnSponsor Grant Call!!", "Isto é definitivamente uma grant call. Terás de fazer montes de coisas e nem vais ser pago!", datePast1, datePast2, mutableListOf("Isto é um requirement", "E isto é outro"), 200.0, questionList.map { it.toDAO() }, sponsor2, mutableListOf())
        //val call3 = GrantCallDAO(0, "TerrySponsor Grant Call!!", "Isto é definitivamente uma grant call. Terás de fazer montes de coisas e nem vais ser pago!", datePast1, datePast2, mutableListOf("Isto é um requirement", "E isto é outro"), 200.0, questionList.map { it.toDAO() }, sponsor3, mutableListOf())


        val call1 = GrantCallDAO(0, "Bolsa Amazónia Jefe Beijos", "Na Amazónia podes desenvolver skills de caixas de cartão!", datePast1, dateFuture, mutableListOf("Boa skill com caixas", "Nao sei mais"), 200.0, questionList1, sponsor1, mutableListOf())
        val call2 = GrantCallDAO(0, "Estagio Miksoft Portas", "Precisamos de um estagiario para tirar cafes", datePast1, datePast2, mutableListOf("Bom com estruturas de dados", "Nao podes gostar da apple"), 300.0, questionList2, sponsor2, mutableListOf())
        val call3 = GrantCallDAO(0, "Bolsa CIAI 20", "Por favor deem-nos uma boa nota, isto custou muito", datePast1, datePast2, mutableListOf("Ser bom aluno", "Desde pequeno que so sonhas com a bola"), 400.0, questionList3, sponsor3, mutableListOf())


        //Save some Grant Calls into the memory db
        calls.save(call1);
        calls.save(call2);
        calls.save(call3);


        // Save a reviewer
        val reviewer1 = ReviewerDAO(0, "Rua Boa", "MikeReviewer", "mikereviewer", "mikereviewer@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "Assistant Professor", institutionRepo.findById(3).get(), emptyList(), emptyList() )
        val reviewer2 = ReviewerDAO(0, "Avenida Txi", "JohnReviewer", "johnreviewer", "johnreviewer@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "Full Professor", institutionRepo.findById(3).get(), emptyList(), emptyList() )
        val reviewer3 = ReviewerDAO(0, "Largo do Malaquias", "TerryReviewer", "terryreviewer", "terryreviewer@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "Mafioso", institutionRepo.findById(2).get(), emptyList(), emptyList() )

        reviewers.save(reviewer1);
        reviewers.save(reviewer2);
        reviewers.save(reviewer3);


        val reviewerForTests = ReviewerDAO(0, "Rua do Osso Buco", "CIAIReviewer", "ciaireviewer", "ciaireviewer@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "DaMafia", institutionRepo.findById(2).get(), emptyList(), emptyList() )
        reviewers.save(reviewerForTests);


        //Save panel chairs
        val reviewerChair1 = ReviewerDAO(0, "Rua Boa", "MikeChair", "mikechair", "mikechair@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "Assistant Professor", institutionRepo.findById(3).get())
        val reviewerChair2 = ReviewerDAO(0, "Avenida Txi", "JohnChair", "johnchair", "johnchair@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "Full Professor", institutionRepo.findById(3).get())
        val reviewerChair3 = ReviewerDAO(0, "Largo do Malaquias", "TerryChair", "terrychair", "terrychair@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "Mafioso", institutionRepo.findById(2).get())

        reviewers.save(reviewerChair1);
        reviewers.save(reviewerChair2);
        reviewers.save(reviewerChair3);

        val panelChairForTests = ReviewerDAO(0, "Avenida das Boas Festas", "CIAIChair", "ciaichair", "ciaichair@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_REVIEWER, "MIEI", "Corrado", institutionRepo.findById(2).get())
        reviewers.save(panelChairForTests)


        //System.out.println("CHAIR: " + reviewers.findById("mikechair").get())



        var panel1 = EvaluationPanelDAO(0, mutableListOf(reviewer1, reviewerForTests, reviewerChair1, reviewerChair2), reviewerChair1, call1)
        var panel2 = EvaluationPanelDAO(0, mutableListOf(reviewer2, reviewerForTests, reviewerChair2, reviewerChair1), reviewerChair2, call2)
        var panel3 = EvaluationPanelDAO(0, mutableListOf(reviewer3, reviewerForTests, reviewerChair3), reviewerChair3, call3)

        //calls.defineEvaluationPanel(8, panel1)
        //calls.defineEvaluationPanel(12, panel2)
        //calls.defineEvaluationPanel(16, panel3)

        evalPanels.save(panel1)
        //callService.defineEvaluationPanel(8, panel1)
        evalPanels.save(panel2)
        evalPanels.save(panel3)

        val student1 = StudentDAO(0, "Rua Boa", "PeterStudent", "peterstudent", "peterstudent@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_STUDENT, 50505, "MIEI", "MSc", institutionRepo.findById(1).get())
        val student2 = StudentDAO(0, "Rua Leve", "KikoStudent", "kikostudent", "kikostudent@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_STUDENT, 50505, "MIEI", "MSc", institutionRepo.findById(1).get())
        val student3 = StudentDAO(0, "Rua Pesada", "PaulStudent", "paulstudent", "paulstudent@gmail", "123", BCryptPasswordEncoder().encode("123"), Roles.ROLE_STUDENT, 50505, "MIEI", "MSc", institutionRepo.findById(1).get())


        //Create students
        students.save(student1);
        students.save(student2);
        students.save(student3);


        //CV answers
        var answerCVPeter1 = AnswerFieldDAO(0, "ok", questions.getOne(4))
        var answerCVPeter2 = AnswerFieldDAO(0, "ok", questions.getOne(5))
        var answerCVPeter3 = AnswerFieldDAO(0, "ok", questions.getOne(6))
        var answerCVPeter4 = AnswerFieldDAO(0, "ok", questions.getOne(7))

        var answerCVKiko1 = AnswerFieldDAO(0, "ok", questions.getOne(4))
        var answerCVKiko2 = AnswerFieldDAO(0, "ok", questions.getOne(5))
        var answerCVKiko3 = AnswerFieldDAO(0, "ok", questions.getOne(6))
        var answerCVKiko4 = AnswerFieldDAO(0, "ok", questions.getOne(7))

        var answerCVPaul1= AnswerFieldDAO(0, "ok", questions.getOne(4))
        var answerCVPaul2 = AnswerFieldDAO(0, "ok", questions.getOne(5))
        var answerCVPaul3 = AnswerFieldDAO(0, "ok", questions.getOne(6))

        //Create CVs
        var cvPeterStudent = CVDAO(0, mutableListOf<AnswerFieldDAO>(answerCVPeter1, answerCVPeter2, answerCVPeter3, answerCVPeter4))
        var cvKikoStudent = CVDAO(0, mutableListOf<AnswerFieldDAO>(answerCVKiko1, answerCVKiko2, answerCVKiko3, answerCVKiko4))
        var cvPaulStudent = CVDAO(0, mutableListOf<AnswerFieldDAO>(answerCVPaul1, answerCVPaul2, answerCVPaul3))

        //Add CVs
        student1.CV = cvPeterStudent
        student2.CV =cvKikoStudent
        student3.CV = cvPaulStudent

        //Update students
        students.save(student1);
        students.save(student2);
        students.save(student3);


        //Create application answers
        //var answer1 = AnswerFieldDTO("Yes", 9)
        //var answer2 = AnswerFieldDTO("Yes", 10)
        //var answer3 = AnswerFieldDTO("no", 11)


        //var answerList = mutableListOf<AnswerFieldDTO>(answer1, answer2, answer3)

        var answer1 = AnswerFieldDAO(0, "Yes", questionList1[0])
        var answer2 = AnswerFieldDAO(0, "Yes", questionList1[1])
        var answer3 = AnswerFieldDAO(0, "no", questionList1[2])

        var answerList1 = mutableListOf<AnswerFieldDAO>(answer1, answer2, answer3)

        var answer4 = AnswerFieldDAO(0, "Wii", questionList1[0])
        var answer5 = AnswerFieldDAO(0, "Ps4", questionList1[1])
        var answer6 = AnswerFieldDAO(0, "yes", questionList1[2])

        var answerList2 = mutableListOf<AnswerFieldDAO>(answer4, answer5, answer6)

        //Create Applications
        //Open
        val applicationOpen1 = GrantApplicationDAO(0, Date(), 0, student1, call1, answerList1 )
        val applicationOpen2 = GrantApplicationDAO(0, Date(), 1, student2, call1, answerList2 )

        applications.save(applicationOpen1)
        applications.save(applicationOpen2)

        val reviewOpen1 = ReviewDAO(0, "too badi", 5f, reviewer1, applicationOpen1)
        val reviewOpen2 = ReviewDAO(0, "so gudi", 20f, reviewer1, applicationOpen1)

        reviews.save(reviewOpen1)
        reviews.save(reviewOpen2)

        //Closed
        var datePast3 = Date()
        datePast3.setTime(1602763200000)

        var answer7 = AnswerFieldDAO(0, "19,99", questionList2[0])
        var answer8 = AnswerFieldDAO(0, "Diploma.bom", questionList2[1])
        var answer9 = AnswerFieldDAO(0, "yes", questionList2[2])

        var answerList3 = mutableListOf<AnswerFieldDAO>(answer7, answer8, answer9)

        val applicationClosed1 = GrantApplicationDAO(0, datePast3, 2, student3, call2, answerList3)

        applications.save(applicationClosed1)

        val reviewClosed1 = ReviewDAO(0, "too good", 20f, reviewer2, applicationClosed1)

        reviews.save(reviewClosed1)

        val finalEvaluation1 = FinalEvaluationDAO(0, "very very good", 20f, reviewerChair2, true, applicationClosed1)

        //applicationClosed1.finalEvaluation = finalEvaluation1
        evaluations.save(finalEvaluation1)
        //evaluations.save(finalEvaluation1)

        var answer10 = AnswerFieldDAO(0, "Nice", questionList2[0])
        var answer11 = AnswerFieldDAO(0, "Diploma.fixe", questionList2[1])
        var answer12 = AnswerFieldDAO(0, "yes", questionList2[2])

        var answerList4 = mutableListOf<AnswerFieldDAO>(answer10, answer11, answer12)

        val applicationClosed2 = GrantApplicationDAO(0, datePast3, 2, student2, call2, answerList4)

        applications.save(applicationClosed2)

        val reviewClosed2 = ReviewDAO(0, "Mediocre, meu puto.", 11f, reviewer2, applicationClosed2)

        reviews.save(reviewClosed2)

        val finalEvaluation2 = FinalEvaluationDAO(0, "Noti goodi enoughi.", 13f, reviewerChair2, false, applicationClosed2)

        //applicationClosed1.finalEvaluation = finalEvaluation1
        evaluations.save(finalEvaluation2)
        //evaluations.save(finalEvaluation1)



    }



}