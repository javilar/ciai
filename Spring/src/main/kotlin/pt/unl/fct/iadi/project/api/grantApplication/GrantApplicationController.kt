package pt.unl.fct.iadi.project.api.grantApplication

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DAO.GrantApplicationDAO
import pt.unl.fct.iadi.project.services.DAO.ReviewerDAO
import pt.unl.fct.iadi.project.services.DTOs.FinalEvaluationDTO
import pt.unl.fct.iadi.project.services.DTOs.GrantApplicationDTO
import pt.unl.fct.iadi.project.services.DTOs.GrantApplicationDetailsDTO
import pt.unl.fct.iadi.project.services.DTOs.ReviewDTO
import pt.unl.fct.iadi.project.services.GrantApplicationService
import pt.unl.fct.iadi.project.services.ReviewerService

/**
 *  Controller Class for Grant Applications.
 *  Should be focused on implementing web services and nothing else (like business logic).
 */

@RestController
class GrantApplicationController(val applications: GrantApplicationService, val reviewers: ReviewerService) : GrantApplicationAPI {

    override fun getAll() = applications.getAll().map { GrantApplicationDetailsDTO(it) }

    override fun getOne(id: Long) = GrantApplicationDetailsDTO(applications.getOne(id))

    override fun update(id: Long, application: GrantApplicationDTO) {
        val applicationToUpdate: GrantApplicationDAO = applications.getOne(id)
        applications.update(application.updateDAO(applicationToUpdate))
    }

    override fun delete(id: Long): Unit = applications.delete(id)

    override fun getFinalEvaluation(id: Long): FinalEvaluationDTO =
            FinalEvaluationDTO(applications.getFinalEvaluationByApplicationId(id))

    override fun submitFinalEvaluation(id: Long, finalEvaluation: FinalEvaluationDTO) {
        val reviewer: ReviewerDAO = reviewers.getOne(finalEvaluation.reviewerUsername)
        val application: GrantApplicationDAO = applications.getOne(id)
        applications.submitFinalEvaluation(id, finalEvaluation.toDAO(reviewer, application))
    }


    override fun getApplicationReviews(id: Long) = applications.getApplicationReviews(id).map { ReviewDTO(it) }

    override fun submitReview(id: Long, review: ReviewDTO) {
        val reviewer: ReviewerDAO = reviewers.getOne(review.reviewerUsername)
        val application: GrantApplicationDAO = applications.getOne(id)
        applications.submitReview(id, review.toDAO(reviewer, application))
    }


}