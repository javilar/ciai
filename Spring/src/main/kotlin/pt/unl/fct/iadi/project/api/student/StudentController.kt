package pt.unl.fct.iadi.project.api.student

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import pt.unl.fct.iadi.project.services.DTOs.*
import pt.unl.fct.iadi.project.services.DTOs.dataItems.AnswerFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.users.StudentDTO
import pt.unl.fct.iadi.project.services.DTOs.users.StudentDetailsDTO

@RestController
class StudentController(val students: StudentService,
                        val applications: GrantApplicationService,
                        val calls: GrantCallService,
                        val questions: QuestionFieldService,
                        val institutions: InstitutionService
) : StudentAPI {

    override fun getAll(): List<StudentDetailsDTO> = students.getAll().map { StudentDetailsDTO(it) }

    override fun getOne(username: String): StudentDetailsDTO = StudentDetailsDTO(students.getOne(username))

    override fun create(student: StudentDTO): Unit =
            students.create(student.toDAO(institutions.getOne(student.institutionId)))

    override fun update(username: String, student: StudentDTO) {
        val studentToUpdate: StudentDAO = students.getOne(username)
        students.update(student.updateDAO(studentToUpdate))
    }

    override fun delete(username: String): Unit = students.delete(username)

    override fun createGrantApplication(username: String, application: GrantApplicationDTO) {
        applications.addOne(application.toDAO(students.getOne(username), calls.getOne(application.grantCallId)))
    }

    override fun getStudentApplications(username: String): List<GrantApplicationDetailsDTO> =
            students.getStudentApplications(username).map { GrantApplicationDetailsDTO(it) }

    override fun createCV(username: String, CV: CVDTO): Unit =
            students.createCV(username, CV.toDAO(CV.answerFields.map { questions.getOne(it.questionID)}))

    override fun getCV(username: String): CVDetailsDTO = CVDetailsDTO(students.getCVByStudentId(username))

    override fun updateCV(username: String, CVFields: List<AnswerFieldDTO>): Unit =
            students.updateCV(username, CVFields.map { it.toDAO(questions.getOne(it.questionID)) })

}