package pt.unl.fct.iadi.project.api.student

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.security.SecurityAnnotation
import pt.unl.fct.iadi.project.services.DTOs.*
import pt.unl.fct.iadi.project.services.DTOs.dataItems.AnswerFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.users.StudentDTO
import pt.unl.fct.iadi.project.services.DTOs.users.StudentDetailsDTO

/**
 * Interface for the Student Controller Class.
 */

@Api(value = "Students", description = "The Students API", tags = ["Students"])
@RequestMapping("/students")
interface StudentAPI {

    @ApiOperation(value = "Get the list of students that are currently in the system")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of all students in the system"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("")
    fun getAll(): List<StudentDetailsDTO>

    @ApiOperation(value = "Get the student with a given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the student with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}")
    fun getOne(@PathVariable username: String): StudentDetailsDTO

    @ApiOperation(value = "Create a new Student in the system")
    @ApiResponses(
            ApiResponse(code = 201, message = "Successfully created a new Student in the system"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    fun create(@RequestBody student: StudentDTO)


    @ApiOperation(value = "Update an existing Student in the system with given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated the Student with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @PutMapping("/{username}")
    @SecurityAnnotation.IsStudentAllowed
    fun update(@PathVariable username: String, @RequestBody student: StudentDTO)


    @ApiOperation(value = "Delete an existing Student in the system with given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated the Student with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @DeleteMapping("/{username}")
    @SecurityAnnotation.IsStudentAllowed
    fun delete(@PathVariable username: String)


    @ApiOperation(value = "Create a Grant Application", nickname = "createApplication")
    @ApiResponses(
            ApiResponse(code = 201, message = "Successfully created a new Grant Application"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/{username}/applications")
    @SecurityAnnotation.IsStudentAllowed
    fun createGrantApplication(@PathVariable username: String, @RequestBody application: GrantApplicationDTO)


    @ApiOperation(value = "List the Grant Applications of the Student with given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully listed the Grant Applications of the Student with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}/applications")
    @SecurityAnnotation.IsStudentAllowed
    fun getStudentApplications(@PathVariable username: String): List<GrantApplicationDetailsDTO>


    @ApiOperation(value = "Create the CV of the student with a given ID")
    @ApiResponses(
            ApiResponse(code = 201, message = "Successfully created the CV of the Student with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/{username}/CV")
    @SecurityAnnotation.IsStudentAllowed
    fun createCV(@PathVariable username: String, @RequestBody CV: CVDTO)

    @ApiOperation(value = "Get the CV of a student with a given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the CV of the Student with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}/CV")
    @SecurityAnnotation.IsStudentAllowed
    fun getCV(@PathVariable username: String): CVDetailsDTO

    @ApiOperation(value = "Update the CV of the student with a given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated the CV of the Student with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @PutMapping("/{username}/CV")
    @SecurityAnnotation.IsStudentAllowed
    fun updateCV(@PathVariable username: String, @RequestBody CVFields: List<AnswerFieldDTO>)

}