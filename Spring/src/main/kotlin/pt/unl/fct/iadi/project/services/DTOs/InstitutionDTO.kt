package pt.unl.fct.iadi.project.services.DTOs

import pt.unl.fct.iadi.project.services.DAO.InstitutionDAO

data class InstitutionDTO(val name: String,
                          val phoneNumber: String)
{
    constructor(it: InstitutionDAO): this(it.name, it.phoneNumber)

    fun toDAO(): InstitutionDAO = InstitutionDAO(0, name, phoneNumber)
}