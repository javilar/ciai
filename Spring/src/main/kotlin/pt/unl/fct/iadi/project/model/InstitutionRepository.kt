package pt.unl.fct.iadi.project.model

import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.GrantCallDAO
import pt.unl.fct.iadi.project.services.DAO.InstitutionDAO

interface InstitutionRepository: CrudRepository<InstitutionDAO, Long> {
}