package pt.unl.fct.iadi.project.model

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.ReviewerDAO
import java.util.*

interface ReviewerRepository: CrudRepository<ReviewerDAO, String> {


    @Query("select r from ReviewerDAO r inner join fetch r.reviews where r.username = :username")
    fun findByIdWithReviews(username: String): Optional<ReviewerDAO>

}