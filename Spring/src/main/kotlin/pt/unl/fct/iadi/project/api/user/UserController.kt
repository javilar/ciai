package pt.unl.fct.iadi.project.api.user

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DTOs.users.UserDetailsDTO
import pt.unl.fct.iadi.project.services.UserService

@RestController
class UserController(val users: UserService) : UserAPI {

    override fun getAll(): List<UserDetailsDTO> = users.getAll().map { UserDetailsDTO(it) }

    override fun getOne(username: String): UserDetailsDTO = UserDetailsDTO(users.getOne(username))

}