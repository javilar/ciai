package pt.unl.fct.iadi.project.services.DTOs.users

import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.DAO.SponsorDAO

data class SponsorDTO(
        override val address:String,
        override val name: String,
        override val username: String,
        override val email: String,
        override val phoneNumber: String,
        override val password: String
) : UserDTO(address, name, username, email, phoneNumber, password, Roles.ROLE_SPONSOR)
{
    constructor(it: SponsorDAO): this(it.address, it.name, it.username, it.email, it.phoneNumber, it.password)

    fun toDAO(): SponsorDAO = SponsorDAO(0, address, name, username, email, phoneNumber, password, Roles.ROLE_SPONSOR)

}

data class SponsorDetailsDTO(
        override val id: Long,
        override val address:String,
        override val name: String,
        override val username: String,
        override val email: String,
        override val phoneNumber: String
) : UserDetailsDTO(id, address, name, username, email, phoneNumber, Roles.ROLE_SPONSOR)
{
    constructor(it: SponsorDAO): this(it.id, it.address, it.name, it.username, it.email, it.phoneNumber)
}