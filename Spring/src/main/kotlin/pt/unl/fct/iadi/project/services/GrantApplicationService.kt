package pt.unl.fct.iadi.project.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.GrantApplicationRepository
import pt.unl.fct.iadi.project.model.FinalEvaluationRepository
import pt.unl.fct.iadi.project.model.ReviewRepository
import pt.unl.fct.iadi.project.services.DAO.*

@Service
class GrantApplicationService(val applications: GrantApplicationRepository,
                              val evaluations: FinalEvaluationRepository,
                              val reviews: ReviewRepository) {

    fun getAll(): Iterable<GrantApplicationDAO>{
            return applications.findAll()
    }

    fun getOne(id: Long): GrantApplicationDAO =
            applications.findById(id).orElseThrow {
                NotFoundException("application with id $id not found")
            }

    fun addOne(application: GrantApplicationDAO){
        val applicationId = application.id
        applications.findById(applicationId).ifPresent {
            throw AlreadyExistsException("Grant Application with id $applicationId already exists")
        }

        applications.findApplicationToCallWithStudent(application.grantCall.id, application.student.username).ifPresent {
            throw StudentAlreadySubmittedApplicationToCall("Student has already submitted an application to this call")
        }

        if(application.student.CV == null)
            throw StudentDoesNotHaveCVException("Student associated to application does not have CV!")

        val call = application.grantCall

        if(!application.submissionDate.after(call.openingDate))
            throw GrantCallNotOpenYetException("Grant call with id ${call.id} is yet to open")
        else if(!application.submissionDate.before(call.closingDate)) {
            System.out.println("App date: " + application.submissionDate)
            System.out.println("Call date: " + call.closingDate)
            throw GrantCallAlreadyClosedException("Grant call with id ${call.id} has already closed")
        }

        applications.save(application)
    }

    fun update(application: GrantApplicationDAO) {
        val applicationId = application.id
        applications.findById(applicationId).orElseThrow {
            NotFoundException("Grant Application with id $applicationId not found")
        }

        applications.save(application)
    }

    fun delete(id: Long) {
        applications.findById(id).orElseThrow {
            NotFoundException("Grant Application with id $id not found")
        }

        applications.deleteById(id)
    }

    fun submitFinalEvaluation(id: Long, finalEvaluation: FinalEvaluationDAO) {
        val application = getOne(id)

        if(application.hasFinalEvaluation())
            throw AlreadyExistsException("Final evaluation for application with id $id already exists")

        if(application.grantCall.evaluationPanel == null)
            throw NotFoundException("application with id $id associated with call " +
                    "${application.grantCall.id} does not have an evaluation panel associated yet")

        finalEvaluation.reviewer.evaluationPanels.find { application.grantCall.evaluationPanel!!.id == it.id }
                ?: throw ReviewerIsNotInEvaluationPanel("Reviewer is not in the evaluation panel of this call")

        //application.finalEvaluation = finalEvaluation
        evaluations.save(finalEvaluation)

        application.status = 2
        applications.save(application)
    }


    fun getFinalEvaluationByApplicationId(id: Long): FinalEvaluationDAO {
       val application = applications.findByIdWithFinalEvaluation(id).orElseThrow {
           NotFoundException("application with id $id not found")
       }

        if(!application.hasFinalEvaluation())
            throw NotFoundException("application with id $id does not have a final evaluation yet")

        return application.finalEvaluation!!

    }

    fun getApplicationReviews(id: Long): List<ReviewDAO> {
        val application: GrantApplicationDAO = applications.findByIdWithReviews(id)
                .orElseThrow { NotFoundException("application with id $id not found") }

        return application.reviews
    }

    fun submitReview(id:Long, review: ReviewDAO) {
        reviews.findById(review.id).ifPresent {
            throw AlreadyExistsException("Review with id ${review.id} already exists")
        }

        val application: GrantApplicationDAO = getOne(id)

        if(application.reviews.find { it.reviewer.username == review.reviewer.username } != null)
                throw AlreadyExistsException("Reviewer already has a review for this application")

        if(!application.grantCall.hasEvaluationPanel())
            throw NotFoundException("application with id $id associated with call " +
                    "${application.grantCall.id} does not have an evaluation panel associated yet")


        if(application.student.institution.id == review.reviewer.institution.id)
            throw ReviewerAndStudentFromSameInstitution("Reviewer needs to be from a different institution than the student")

        review.reviewer.evaluationPanels.find { application.grantCall.evaluationPanel!!.id == it.id }
                ?: throw ReviewerIsNotInEvaluationPanel("Reviewer is not in the evaluation panel of this call")


        reviews.save(review)
    }

}