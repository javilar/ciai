package pt.unl.fct.iadi.project.security

import org.springframework.security.access.prepost.PreAuthorize

class SecurityAnnotation {

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN')")
    annotation class IsAdmin

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('STUDENT') or hasRole('ADMIN')")
    annotation class IsStudent

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('REVIEWER') or hasRole('ADMIN')")
    annotation class IsReviewer

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('SPONSOR')")
    annotation class IsSponsor

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (hasRole('STUDENT') and @SecurityService.canStudent(authentication.principal.getUsername(), #username))")
    annotation class IsStudentAllowed


    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (hasRole('STUDENT') and @SecurityService.canManageApplication(authentication.principal.getUsername(), #id))")
    annotation class IsAllowedManageApplication

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (hasRole('REVIEWER') and @SecurityService.canSubmitFinalEvaluation(authentication.principal.getUsername(), #id))")
    annotation class IsAllowedSubmitFinalEvaluation

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (hasRole('REVIEWER') and @SecurityService.canSubmitReview(authentication.principal.getUsername(), #id))")
    annotation class IsAllowedSubmitReview

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (((hasRole('REVIEWER') or (hasRole('STUDENT'))) and @SecurityService.canViewGrantApplication(authentication.principal.getUsername(), #id)) or @SecurityService.canViewGrantApplication(#id))")
    annotation class IsAllowedViewApplication

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (((hasRole('REVIEWER') or (hasRole('STUDENT'))) and @SecurityService.canViewGrantApplicationReviews(authentication.principal.getUsername(), #id)) or @SecurityService.canViewGrantApplicationReviews(#id))")
    annotation class IsAllowedViewApplicationReviews

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (hasRole('REVIEWER') and @SecurityService.canViewReviewersReview(authentication.principal.getUsername(), #username))")
    annotation class IsAllowedViewReviewersReviews

    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    @MustBeDocumented
    @PreAuthorize("hasRole('ADMIN') or (hasRole('REVIEWER') and @SecurityService.canViewReview(authentication.principal.getUsername(), #id))")
    annotation class IsAllowedViewReview

}