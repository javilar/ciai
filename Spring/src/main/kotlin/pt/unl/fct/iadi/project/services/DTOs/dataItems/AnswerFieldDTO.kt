package pt.unl.fct.iadi.project.services.DTOs.dataItems

import pt.unl.fct.iadi.project.services.DAO.AnswerFieldDAO
import pt.unl.fct.iadi.project.services.DAO.QuestionFieldDAO


data class AnswerFieldDTO(val dataItem: String,
                          val questionID: Long)
{
    constructor(it: AnswerFieldDAO): this(it.dataItem, it.question.id) //remove later

    fun toDAO(question: QuestionFieldDAO): AnswerFieldDAO = AnswerFieldDAO(0, dataItem, question)
}

data class AnswerFieldDetailsDTO(val id: Long,
                                 val dataItem: String,
                                 val question: QuestionFieldDetailsDTO)
{
    constructor(it: AnswerFieldDAO): this(it.id, it.dataItem, QuestionFieldDetailsDTO(it.question))
}
