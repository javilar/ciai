package pt.unl.fct.iadi.project.api.reviewLogic.reviewers

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DTOs.GrantApplicationDetailsDTO
import pt.unl.fct.iadi.project.services.DTOs.ReviewDTO
import pt.unl.fct.iadi.project.services.DTOs.users.ReviewerDTO
import pt.unl.fct.iadi.project.services.DTOs.users.ReviewerDetailsDTO
import pt.unl.fct.iadi.project.services.InstitutionService
import pt.unl.fct.iadi.project.services.ReviewerService

@RestController
class ReviewersController(var reviewers : ReviewerService, var institutions: InstitutionService) : ReviewersAPI {

    override fun getOne(username: String): ReviewerDetailsDTO =
            ReviewerDetailsDTO(reviewers.getOne(username))

    override fun create(reviewer: ReviewerDTO): Unit =
            reviewers.create(reviewer.toDAO(institutions.getOne(reviewer.institutionId)))

    override fun getReviews(username: String): List<ReviewDTO> =
            reviewers.getReviewerReviews(username).map { ReviewDTO(it) }

    override fun getReviewerApplications(username: String): List<GrantApplicationDetailsDTO> =
            reviewers.getReviewerApplications(username).map { GrantApplicationDetailsDTO(it) }


    override fun getPanelChairApplications(username: String): List<GrantApplicationDetailsDTO> =
            reviewers.getPanelChairApplications(username).map { GrantApplicationDetailsDTO(it) }

}