package pt.unl.fct.iadi.project.services.DTOs.dataItems

import pt.unl.fct.iadi.project.services.DAO.QuestionFieldDAO

data class QuestionFieldDTO(val title: String,
                            val description: String,
                            val mandatory: Boolean,
                            val datatype: String)
{
    constructor(it: QuestionFieldDAO): this(it.title, it.description, it.mandatory, it.Datatype)

    fun toDAO(): QuestionFieldDAO = QuestionFieldDAO(0, title, description, mandatory, datatype)
}

data class QuestionFieldDetailsDTO(
                            val id: Long,
                            val title: String,
                            val description: String,
                            val mandatory: Boolean,
                            val Datatype: String)
{
    constructor(it: QuestionFieldDAO): this(it.id, it.title, it.description, it.mandatory, it.Datatype)
}