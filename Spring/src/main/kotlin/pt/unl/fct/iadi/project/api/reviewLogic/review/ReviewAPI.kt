package pt.unl.fct.iadi.project.api.reviewLogic.review

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.security.SecurityAnnotation
import pt.unl.fct.iadi.project.services.DTOs.ReviewDTO

@Api(value = "Reviews", description = "The Reviews API", tags = ["Reviews"])
@RequestMapping("/reviews")
interface ReviewAPI {

    @ApiOperation(value = "Get the Review with given ID", nickname = "getReview")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved Review with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}", produces = ["application/json"])
    @SecurityAnnotation.IsAllowedViewReview
    fun getOne(@PathVariable id: Long): ReviewDTO

}