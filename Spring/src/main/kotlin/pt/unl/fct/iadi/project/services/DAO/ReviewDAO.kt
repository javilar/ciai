package pt.unl.fct.iadi.project.services.DAO

import javax.persistence.*

@Entity
data class ReviewDAO(
        @Id
        @GeneratedValue
        var id: Long = 0,
        @Lob
        @Column
        var evaluation:String = "",
        var score: Float = 0f,

        @ManyToOne
        var reviewer: ReviewerDAO = ReviewerDAO(),

        @ManyToOne
        var application: GrantApplicationDAO = GrantApplicationDAO()

)
{
}