package pt.unl.fct.iadi.project.api.grantApplication

import io.swagger.annotations.*
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.project.services.DTOs.FinalEvaluationDTO
import pt.unl.fct.iadi.project.services.DTOs.ReviewDTO
import pt.unl.fct.iadi.project.config.SwaggerConfiguration.Companion.RESPONSE_FORBIDDEN
import pt.unl.fct.iadi.project.config.SwaggerConfiguration.Companion.RESPONSE_NOT_FOUND
import pt.unl.fct.iadi.project.config.SwaggerConfiguration.Companion.RESPONSE_UNAUTHORIZED
import pt.unl.fct.iadi.project.security.SecurityAnnotation
import pt.unl.fct.iadi.project.services.DTOs.GrantApplicationDTO
import pt.unl.fct.iadi.project.services.DTOs.GrantApplicationDetailsDTO

/**
 * Interface for the Grant Application Controller Class.
 * Makes the Controller Class cleaner.
 * The annotations are kept in the interface and not in the class, it just implements the methods.
 */

@Api(value = "Grant Applications", description = "The Grant Applications API", tags = ["Grant Applications"])
@RequestMapping("/grantApplications")
interface GrantApplicationAPI {

    @ApiOperation(value = "Get the list of Grant Applications in the system", nickname = "getAllApplications")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list of Grant Applications"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )
    @GetMapping("", produces = ["application/json"])
    @SecurityAnnotation.IsAdmin
    fun getAll(): List<GrantApplicationDetailsDTO>


    @ApiOperation(value = "Get the Grant Application with given ID", nickname = "getApplication")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved Grant Application with given ID"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}", produces = ["application/json"])
    @SecurityAnnotation.IsAllowedViewApplication
    fun getOne(@PathVariable id: Long): GrantApplicationDetailsDTO

    @ApiOperation(value = "Update an existing Grant Application with given ID", nickname = "updateApplication")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated the Grant Application with given ID"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )
    @PutMapping("/{id}")
    @SecurityAnnotation.IsAllowedManageApplication
    fun update(@PathVariable id: Long, @RequestBody application: GrantApplicationDTO)


    @ApiOperation(value = "Delete the Grant Application with given ID", nickname = "deleteApplication")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted the Grant Application with given ID"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )
    @DeleteMapping("/{id}", produces = ["application/json"])
    @SecurityAnnotation.IsAllowedManageApplication
    fun delete(@PathVariable id: Long)


    @ApiOperation(value = "Submits a review", nickname = "submitReview")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully submited a Review"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )
    @PostMapping("{id}/reviews")
    @SecurityAnnotation.IsAllowedSubmitReview
    fun submitReview(@PathVariable id: Long, @RequestBody review: ReviewDTO)


    @ApiOperation(value = "Get the final evaluation of the Grant Application with given ID", nickname = "deleteApplication")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the final evaluation with given ID"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )

    @GetMapping("/{id}/finalEvaluation", produces = ["application/json"])
    @SecurityAnnotation.IsAllowedViewApplication
    fun getFinalEvaluation(@PathVariable id: Long): FinalEvaluationDTO

    @ApiOperation(value = "Submits a final evaluation to an application", nickname = "submitFinalEvaluation")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully submitted a Final Evaluation to the application"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )
    @PostMapping("{id}/finalEvaluation")
    @SecurityAnnotation.IsAllowedSubmitFinalEvaluation
    fun submitFinalEvaluation(@PathVariable id: Long, @RequestBody finalEvaluation: FinalEvaluationDTO)

    @ApiOperation(value = "Get the Reviews of the Grant Application with given ID", nickname = "getApplicationReviews")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved Reviews of the Grant Application with given ID"),
            ApiResponse(code = 401, message = RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}/reviews", produces = ["application/json"])
    @SecurityAnnotation.IsAllowedViewApplicationReviews
    fun getApplicationReviews(@PathVariable id: Long): List<ReviewDTO>
}