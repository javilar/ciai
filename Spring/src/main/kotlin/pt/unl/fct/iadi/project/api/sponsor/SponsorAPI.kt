package pt.unl.fct.iadi.project.api.sponsor

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.security.SecurityAnnotation
import pt.unl.fct.iadi.project.services.DTOs.*
import pt.unl.fct.iadi.project.services.DTOs.users.SponsorDTO
import pt.unl.fct.iadi.project.services.DTOs.users.SponsorDetailsDTO

@Api(value = "Sponsors", description = "The Sponsors API", tags = ["Sponsors"])
@RequestMapping("/sponsors")
interface SponsorAPI {

    @ApiOperation(value = "Get the list of sponsors that are currently in the system")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of all sponsors in the system"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("")
    fun getAll(): List<SponsorDetailsDTO>

    @ApiOperation(value = "Get the sponsor with a given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the sponsor with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}")
    fun getOne(@PathVariable username: String): SponsorDetailsDTO

    @ApiOperation(value = "Create a new Sponsor in the system")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully created a new Sponsor in the system"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    fun create(@RequestBody sponsor: SponsorDTO)

    @ApiOperation(value = "Create a Grant Call", nickname = "createCall")
    @ApiResponses(
            ApiResponse(code = 201, message = "Successfully created a new Grant Call"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/{username}/calls")
    @SecurityAnnotation.IsSponsor
    fun createGrantCall(@PathVariable username: String, @RequestBody call: GrantCallDTO)

    @ApiOperation(value = "List the Grant Calls of the Sponsor with given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully listed the Grant Calls of the Sponsor with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{username}/calls")
    fun getSponsorCalls(@PathVariable username: String): List<GrantCallDetailsDTO>

}