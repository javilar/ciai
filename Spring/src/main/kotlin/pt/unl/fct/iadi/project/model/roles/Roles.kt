package pt.unl.fct.iadi.project.model.roles

enum class Roles {

        ROLE_STUDENT,
        ROLE_REVIEWER,
        ROLE_SPONSOR,
        ROLE_ADMIN

}