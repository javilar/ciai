package pt.unl.fct.iadi.project.services.DAO

import pt.unl.fct.iadi.project.model.roles.Roles
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
open class UserDAO(

        @GeneratedValue
        open val id: Long = 0,
        open var address:String = "",
        open var name: String = "",

        @Id
        open var username: String = "",

        open var email: String = "",
        open var phoneNumber: String = "",
        open var password: String = "",

        @Enumerated(EnumType.STRING)
        open val role: Roles = Roles.ROLE_STUDENT
)

@Entity
data class StudentDAO(

        @GeneratedValue
        override var id: Long = 0,
        override var address: String = "",
        override var name: String = "",

        @Id
        override var username: String = "",

        override var email: String = "",
        override var phoneNumber: String = "",
        override var password: String = "",
        override var role: Roles = Roles.ROLE_STUDENT,
        var number: Int = 0,
        var course: String = "",
        var degree: String = "",

        @OneToOne
        var institution: InstitutionDAO = InstitutionDAO(),

        @OneToMany(mappedBy = "student", cascade = [CascadeType.ALL])
        var applications: MutableList<GrantApplicationDAO> = mutableListOf(),
        @OneToOne(cascade = [CascadeType.ALL])
        var CV: CVDAO? = null

): UserDAO(id, address, name, username, email, phoneNumber, password, role)

@Entity
data class SponsorDAO(

        @GeneratedValue
        override val id: Long = 0,
        override var address:String = "",
        override var name: String = "",

        @Id
        override var username: String = "",

        override var email: String = "",
        override var phoneNumber: String = "",
        override var password: String = "",
        override var role: Roles = Roles.ROLE_SPONSOR,

        @OneToMany(mappedBy = "sponsor", cascade = [CascadeType.ALL])
        var calls: List<GrantCallDAO> = emptyList()


): UserDAO(id, address, name, username, email, phoneNumber, password, role)

@Entity
data class ReviewerDAO(

        @GeneratedValue
        override val id: Long = 0,
        override var address: String = "",
        override var name: String = "",

        @Id
        override var username: String = "",

        override var email: String = "",
        override var phoneNumber: String = "",
        override var password: String = "",
        override var role: Roles = Roles.ROLE_REVIEWER,
        var course: String = "",
        var position: String = "",

        @OneToOne
        var institution: InstitutionDAO = InstitutionDAO(),

        @OneToMany(mappedBy = "reviewer", cascade = [CascadeType.ALL])
        val reviews: List<ReviewDAO> = emptyList(),

        @ManyToMany(mappedBy = "reviewers")
        val evaluationPanels: List<EvaluationPanelDAO> = emptyList(),

        @OneToMany(mappedBy = "panelChair")
        val evaluationPanelsLed: List<EvaluationPanelDAO> = emptyList()


) : UserDAO(id, address, name, username, email, phoneNumber, password, role)
