package pt.unl.fct.iadi.project.services.DAO

import javax.persistence.*

@Entity
data class FinalEvaluationDAO(

        @Id
        @GeneratedValue
        var id: Long = 0,
        @Lob
        @Column
        var evaluation: String = "",
        var score: Float = 0f,

        @ManyToOne
        var reviewer: ReviewerDAO = ReviewerDAO(),
        var accepted: Boolean = false,

        @OneToOne
        var application: GrantApplicationDAO = GrantApplicationDAO()

)

