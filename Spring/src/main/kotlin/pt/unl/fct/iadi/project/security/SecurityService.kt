package pt.unl.fct.iadi.project.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pt.unl.fct.iadi.project.services.*
import pt.unl.fct.iadi.project.services.DAO.*
import java.util.*

@Component("SecurityService")
class SecurityService {

    @Autowired
    lateinit var users: UserService

    @Autowired
    lateinit var sponsors: SponsorService

    @Autowired
    lateinit var students: StudentService

    @Autowired
    lateinit var reviewers: ReviewerService

    @Autowired
    lateinit var reviews: ReviewService

    @Autowired
    lateinit var applications: GrantApplicationService



    fun  canStudent(username: String, studentUsername: String): Boolean{
        return username.equals(students.getOne(studentUsername).username)
    }


    fun  canManageApplication(username: String, grantApplicationId: Long): Boolean{
        val application: GrantApplicationDAO = applications.getOne(grantApplicationId)
        return application.student.username.equals(username)
    }


    fun canSubmitFinalEvaluation(username: String, grantApplicationId: Long): Boolean {
        val application: GrantApplicationDAO = applications.getOne(grantApplicationId)

        return if(application.grantCall.evaluationPanel != null)
            application.grantCall.evaluationPanel?.panelChair!!.username.equals(username)
        else
            false
    }

    fun canSubmitReview(username: String, grantApplicationId: Long): Boolean {
        val application: GrantApplicationDAO = applications.getOne(grantApplicationId)

        return if(application.grantCall.evaluationPanel != null)
            application.grantCall.evaluationPanel?.reviewers!!.any { it.username.equals(username)}
                    || application.grantCall.evaluationPanel?.panelChair?.username!!.equals(username)
        else
            false
    }

    fun canViewGrantApplication(grantApplicationId: Long): Boolean {
        val application: GrantApplicationDAO = applications.getOne(grantApplicationId)
        val isClosed: Boolean = application.grantCall.openingDate > Date() || application.grantCall.closingDate < Date()

        // if the call is closed and the application was accepted, anyone can see it
        if(isClosed && application.hasFinalEvaluation() && application.finalEvaluation?.accepted!!) {
            return true
        }
        return false
    }

    fun canViewGrantApplication(username: String, grantApplicationId: Long): Boolean {
        val application: GrantApplicationDAO = applications.getOne(grantApplicationId)
        val user: UserDAO = users.getOne(username)

        if(user is ReviewerDAO) {
            if(application.grantCall.evaluationPanel != null) {
                return (application.grantCall.evaluationPanel?.reviewers!!.any { it.username.equals(username) }) || (application.grantCall.evaluationPanel?.panelChair?.username!!.equals(username))
            }
            return false
        }

        if (user is StudentDAO){
            return application.student.username.equals(username)
        }


        return false
    }


    fun canViewGrantApplicationReviews(grantApplicationId: Long): Boolean {
        val application: GrantApplicationDAO = applications.getOne(grantApplicationId)
        val isClosed: Boolean = application.grantCall.openingDate > Date() || application.grantCall.closingDate < Date()

        // if the call is closed and the application was accepted, anyone can see it
        if(isClosed && application.hasFinalEvaluation() && application.finalEvaluation?.accepted!!) {
            return true
        }
        return false
    }

    fun canViewGrantApplicationReviews(username: String, grantApplicationId: Long): Boolean {
        val application: GrantApplicationDAO = applications.getOne(grantApplicationId)
        val user: UserDAO = users.getOne(username)

        if(user is ReviewerDAO) {
            if(application.grantCall.evaluationPanel != null) {
                return (application.grantCall.evaluationPanel?.reviewers!!.any { it.username.equals(username) }) || (application.grantCall.evaluationPanel?.panelChair?.username!!.equals(username))
            }
            return false
        }

        if (user is StudentDAO){
            if(application.hasFinalEvaluation())
                return application.student.username.equals(username)
            return false
        }


        return false
    }



    fun canViewReviewersReview(username: String, reviewerUsername: String): Boolean {
        return reviewers.getOne(reviewerUsername).username.equals(username)
    }

    fun canViewReview(username: String, reviewId: Long): Boolean {
        return reviews.getOne(reviewId).reviewer.username.equals(username)
    }
}