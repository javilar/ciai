package pt.unl.fct.iadi.project.services.DAO

import javax.persistence.*

@Entity
data class EvaluationPanelDAO(
        @Id
        @GeneratedValue
        var id: Long = 0,

        @ManyToMany
        var reviewers: List<ReviewerDAO> = emptyList(),

        @ManyToOne
        var panelChair: ReviewerDAO = ReviewerDAO(),

        @OneToOne
        var call: GrantCallDAO = GrantCallDAO()

)

{
}