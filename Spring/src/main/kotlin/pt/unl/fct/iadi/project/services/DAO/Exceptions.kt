package pt.unl.fct.iadi.project.services.DAO

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFoundException(message:String): RuntimeException(message)

@ResponseStatus(HttpStatus.CONFLICT)
class AlreadyExistsException(message:String = "Already exists!"): RuntimeException(message)

@ResponseStatus(HttpStatus.NOT_FOUND)
class StudentDoesNotHaveCVException(message:String): RuntimeException(message)

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
class ApplicationDoesNotHaveFinalEvaluationException(message:String): RuntimeException(message)

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
class ReviewerIsNotInEvaluationPanel(message:String): RuntimeException(message)

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
class GrantCallNotOpenYetException(message:String): RuntimeException(message)

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
class GrantCallAlreadyClosedException(message:String): RuntimeException(message)

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
class StudentAlreadySubmittedApplicationToCall(message:String): RuntimeException(message)

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
class ReviewerAndStudentFromSameInstitution(message:String): RuntimeException(message)