package pt.unl.fct.iadi.project.model

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.GrantApplicationDAO
import java.util.*

interface GrantApplicationRepository: CrudRepository<GrantApplicationDAO, Long> {

    @Query("select a from GrantApplicationDAO a where a.grantCall.id = ?1 and a.student.username = ?2")
    fun findApplicationToCallWithStudent(callId: Long, studentUsername: String): Optional<GrantApplicationDAO>

    @Query("select a from GrantApplicationDAO a inner join fetch a.finalEvaluation where a.id = :id")
    fun findByIdWithFinalEvaluation(id:Long) : Optional<GrantApplicationDAO>

    @Query("select a from GrantApplicationDAO a inner join fetch a.reviews where a.id = :id")
    fun findByIdWithReviews(id:Long) : Optional<GrantApplicationDAO>

    @Query("select a from GrantApplicationDAO a inner join fetch a.finalEvaluation as f where (a.grantCall.id = :id) and (f.accepted = true)")
    fun findAcceptedApplicationsByCallId(id: Long): Iterable<GrantApplicationDAO>

    @Query("select a from GrantApplicationDAO a inner join fetch a.grantCall as c inner join fetch c.evaluationPanel as e inner join fetch e.reviewers as r where (r.username = :username) and (a.status <> 0)")
    fun findReviewerApplications(username: String): Iterable<GrantApplicationDAO>

    @Query("select a from GrantApplicationDAO a inner join fetch a.grantCall as c inner join fetch c.evaluationPanel as e inner join fetch e.panelChair as p where (p.username = :username) and (a.status <> 0)")
    fun findPanelChairApplications(username: String): Iterable<GrantApplicationDAO>
}