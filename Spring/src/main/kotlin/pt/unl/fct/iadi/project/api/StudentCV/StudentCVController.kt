package pt.unl.fct.iadi.project.api.StudentCV

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DTOs.dataItems.QuestionFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.QuestionFieldDetailsDTO
import pt.unl.fct.iadi.project.services.QuestionFieldService

@RestController
class StudentCVController(val questions: QuestionFieldService) : StudentCVAPI {

    override fun addOneItem(item: QuestionFieldDTO): Unit = questions.addOne(item.toDAO())

    override fun getAllItems(): List<QuestionFieldDetailsDTO> = questions.getAll().map { QuestionFieldDetailsDTO(it) }

}