package pt.unl.fct.iadi.project.api.grantCall

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.services.DTOs.*
import pt.unl.fct.iadi.project.services.DTOs.EvaluationPanelDetailsDTO

@Api(value = "Grant Calls", description = "The Grant Calls API", tags = ["Grant Calls"])
@RequestMapping("/grantCalls")
interface GrantCallAPI {

    @ApiOperation(value = "Get the list of Grant Calls in the system", nickname = "getAllCalls")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list of Grant Calls"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("", produces = ["application/json"])
    fun getAll(): List<GrantCallDetailsDTO>


    @ApiOperation(value = "Get the list of all open Grant Calls in the system", nickname = "getAllOpenCalls")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list of open Grant Calls"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/open", produces = ["application/json"])
    fun getAllOpen(): List<GrantCallDetailsDTO>


    @ApiOperation(value = "Get the Grant Calls with ID", nickname = "getOneCall")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list of Grant Calls"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}", produces = ["application/json"])
    fun getOne(@PathVariable id: Long): GrantCallDetailsDTO


    @ApiOperation(value = "Updates the grant call with the given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated the Grant Call with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @PutMapping("/{id}")
    fun updateGrantCall(@PathVariable id:Long, @RequestBody call: GrantCallDTO)


    @ApiOperation(value = "Deleted the grant call with the given ID")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted the Grant Call with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @DeleteMapping("/{id}", produces = ["application/json"])
    fun deleteGrantCall(@PathVariable id: Long)


    @ApiOperation(value = "Get the list of all applications from the grant call with given ID", nickname = "getGrantCallApplications")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list of applications from Grant Call with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}/applications")
    fun getGrantCallApplications(@PathVariable id: Long): List<GrantApplicationDetailsDTO>


    @ApiOperation(value = "Assign an Evaluation Panel to the grant call given by ID", nickname = "defineEvaluationPanel")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully assigned an Evaluation Panel to the Grant Call with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @PostMapping("/{id}/evaluationPanel")
    fun defineEvaluationPanel(@PathVariable id: Long, @RequestBody evaluationPanel: EvaluationPanelDTO)


    @ApiOperation(value = "Get the Evaluation Panel assigned to the grant call given by ID", nickname = "getEvaluationPanel")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the Evaluation Panel assigned to the Grant Call with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}/evaluationPanel")
    fun getEvaluationPanel(@PathVariable id: Long): EvaluationPanelDetailsDTO


    @ApiOperation(value = "Get the list of all accepted applications from the grant call with given ID", nickname = "getGrantCallApplications")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list of applications from Grant Call with given ID"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("/{id}/acceptedApplications")
    fun getAcceptedGrantCallApplications(@PathVariable id: Long): List<GrantApplicationDetailsDTO>

}