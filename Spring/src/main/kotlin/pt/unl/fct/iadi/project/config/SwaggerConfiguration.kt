package pt.unl.fct.iadi.project.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    companion object {
        const val RESPONSE_UNAUTHORIZED = "You are not authorized to use this resource" //401
        const val RESPONSE_FORBIDDEN = "Accessing the resource you were trying to reach is forbidden" //403
        const val RESPONSE_NOT_FOUND = "The resource you were trying to reach was not found" //404
    }

    @Bean
    fun api(): Docket =
            Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("pt.unl.fct.iadi.project"))
                    .paths(PathSelectors.any())
                    .build().apiInfo(apiEndPointsInfo())

    fun apiEndPointsInfo(): ApiInfo =
            ApiInfoBuilder()
                    .title("IADI 2020")
                    .description("IADI Project 2020 - Grant Application System")
                    .contact(Contact("João Vilar/ Pedro Almeida/ Pedro Agostinho", "http://www.pre.com", ""))
                    .license("Apache 2.0")
                    .licenseUrl("http://www.pre.com")
                    .version("1.0.0")
                    .build()

}