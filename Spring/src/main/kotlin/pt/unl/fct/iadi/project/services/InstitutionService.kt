package pt.unl.fct.iadi.project.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.InstitutionRepository
import pt.unl.fct.iadi.project.services.DAO.InstitutionDAO
import pt.unl.fct.iadi.project.services.DAO.NotFoundException

@Service
class InstitutionService(var institutions: InstitutionRepository) {

    fun getAll(): Iterable<InstitutionDAO> = institutions.findAll()

    fun getOne(id: Long): InstitutionDAO = institutions.findById(id).orElseThrow {
        NotFoundException("Sponsor with id $id not found")
    }

}