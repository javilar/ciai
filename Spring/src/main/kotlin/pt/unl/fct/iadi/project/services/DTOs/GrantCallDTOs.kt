package pt.unl.fct.iadi.project.services.DTOs

import pt.unl.fct.iadi.project.services.DAO.GrantApplicationDAO
import pt.unl.fct.iadi.project.services.DAO.GrantCallDAO
import pt.unl.fct.iadi.project.services.DAO.SponsorDAO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.QuestionFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.QuestionFieldDetailsDTO
import java.util.*

data class GrantCallDTO(val title: String,
                        val description: String,
                        val openingDate: Date,
                        val closingDate: Date,
                        val requirements: List<String>,
                        val funding: Double,
                        val questionFields: List<QuestionFieldDTO>)
{

    fun toDAO(sponsor: SponsorDAO): GrantCallDAO =
            GrantCallDAO(0, title, description, openingDate, closingDate,
                    requirements, funding, questionFields.map { it.toDAO() }, sponsor)

    fun updateDAO(callToUpdate: GrantCallDAO): GrantCallDAO {
        callToUpdate.title = this.title
        callToUpdate.description = this.description
        callToUpdate.openingDate = this.openingDate
        callToUpdate.closingDate = this.closingDate
        callToUpdate.requirements = this.requirements
        callToUpdate.funding = this.funding
        callToUpdate.dataItems = this.questionFields.map { it.toDAO() }

        return callToUpdate
    }
}

data class GrantCallDetailsDTO(val id:Long,
                        val title: String,
                        val description: String,
                        val openingDate: Date,
                        val closingDate: Date,
                        val requirements: List<String>,
                        val funding: Double,
                        val questionFields: List<QuestionFieldDetailsDTO>,
                        val numberOfApplications: Int
)
{
    constructor(it: GrantCallDAO) : this (it.id, it.title, it.description, it.openingDate, it.closingDate,
            it.requirements, it.funding, it.dataItems.map { QuestionFieldDetailsDTO(it) }, it.applications.count{ it.status != 0 })
}