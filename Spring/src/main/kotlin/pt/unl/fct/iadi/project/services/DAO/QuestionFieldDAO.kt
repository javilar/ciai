package pt.unl.fct.iadi.project.services.DAO

import javax.persistence.*

@Entity
data class QuestionFieldDAO(
        @Id
        @GeneratedValue
        var id: Long = 0,
        var title: String = "",
        var description: String = "",
        var mandatory: Boolean = false,
        var Datatype: String = ""
)