package pt.unl.fct.iadi.project.api.grantCall

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DAO.GrantCallDAO
import pt.unl.fct.iadi.project.services.DTOs.*
import pt.unl.fct.iadi.project.services.GrantCallService
import pt.unl.fct.iadi.project.services.ReviewerService

@RestController
class GrantCallController(var grantCalls: GrantCallService, var reviewers: ReviewerService) : GrantCallAPI {

    override fun getAll() =
            grantCalls.getAll().map { GrantCallDetailsDTO(it) }

    override fun getAllOpen(): List<GrantCallDetailsDTO> =
            grantCalls.getAllOpen().map { GrantCallDetailsDTO(it) }

    override fun getOne(id: Long): GrantCallDetailsDTO =
            GrantCallDetailsDTO(grantCalls.getOne(id))

    override fun updateGrantCall(id: Long, call: GrantCallDTO) {
        val callToUpdate: GrantCallDAO = grantCalls.getOne(id)
        grantCalls.updateGrantCall(call.updateDAO(callToUpdate))
    }

    override fun deleteGrantCall(id: Long): Unit =
            grantCalls.deleteGrantCall(id)

    override fun getGrantCallApplications(id: Long): List<GrantApplicationDetailsDTO> =
            grantCalls.getGrantCallApplicationsByCallId(id).map {
                GrantApplicationDetailsDTO(it)
            }

    override fun defineEvaluationPanel(id: Long, evaluationPanel: EvaluationPanelDTO): Unit =
            grantCalls.defineEvaluationPanel(id,
                    evaluationPanel.toDAO(evaluationPanel.reviewerEmails.map { reviewers.getOne(it) },
                            reviewers.getOne(evaluationPanel.panelChairEmail),
                            grantCalls.getOne(id)))

    override fun getEvaluationPanel(id: Long): EvaluationPanelDetailsDTO =
            EvaluationPanelDetailsDTO(grantCalls.getEvaluationPanel(id))


    override fun getAcceptedGrantCallApplications(id: Long): List<GrantApplicationDetailsDTO> =
            grantCalls.getAcceptedGrantCallApplicationsByCallId(id).map {
                GrantApplicationDetailsDTO(it)
            }

}