package pt.unl.fct.iadi.project.api.sponsor

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DTOs.GrantCallDTO
import pt.unl.fct.iadi.project.services.DTOs.GrantCallDetailsDTO
import pt.unl.fct.iadi.project.services.DTOs.users.SponsorDTO
import pt.unl.fct.iadi.project.services.DTOs.users.SponsorDetailsDTO
import pt.unl.fct.iadi.project.services.GrantCallService
import pt.unl.fct.iadi.project.services.SponsorService

@RestController
class SponsorController(val sponsors: SponsorService, val calls: GrantCallService) : SponsorAPI {

    override fun getAll(): List<SponsorDetailsDTO> =
            sponsors.getAll().map { SponsorDetailsDTO(it) }

    override fun getOne(username: String): SponsorDetailsDTO =
            SponsorDetailsDTO(sponsors.getOne(username))

    override fun create(sponsor: SponsorDTO): Unit =
            sponsors.create(sponsor.toDAO())

    override fun createGrantCall(username: String, call: GrantCallDTO) =
            calls.createGrantCall(call.toDAO(sponsors.getOne(username)))

    override fun getSponsorCalls(username: String): List<GrantCallDetailsDTO> =
            sponsors.getSponsorCalls(username).map { GrantCallDetailsDTO(it) }


}