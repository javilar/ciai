package pt.unl.fct.iadi.project.model

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import pt.unl.fct.iadi.project.services.DAO.SponsorDAO
import java.util.*

interface SponsorRepository: CrudRepository<SponsorDAO, String> {

    @Query("select s from SponsorDAO s inner join fetch s.calls where s.username = :username")
    fun findByIdWithCalls(username: String): Optional<SponsorDAO>
}