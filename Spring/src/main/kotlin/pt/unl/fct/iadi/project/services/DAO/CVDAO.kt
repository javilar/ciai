package pt.unl.fct.iadi.project.services.DAO

import javax.persistence.*

@Entity
data class CVDAO(
        @Id
        @GeneratedValue
        var id: Long = 0,

        @OneToMany(cascade = [CascadeType.ALL])
        var answers: MutableList<AnswerFieldDAO> = mutableListOf()

)