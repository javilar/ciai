package pt.unl.fct.iadi.project.services.DTOs.users

import pt.unl.fct.iadi.project.model.roles.Roles
import pt.unl.fct.iadi.project.services.DAO.InstitutionDAO
import pt.unl.fct.iadi.project.services.DAO.ReviewerDAO

data class ReviewerDTO(
        override val address:String,
        override val name: String,
        override val username: String,
        override val email: String,
        override val phoneNumber: String,
        override val password: String,
        val course: String,
        val position: String,
        val institutionId: Long) : UserDTO(address, name, username, email, phoneNumber, password, Roles.ROLE_REVIEWER)

{
    constructor(it: ReviewerDAO): this(it.address, it.name, it.username,
            it.email, it.phoneNumber,it.password, it.course, it.position, it.institution.id)

    fun toDAO(institution: InstitutionDAO): ReviewerDAO =
            ReviewerDAO(0, address, name, username, email, phoneNumber,
                    password, Roles.ROLE_REVIEWER, course, position, institution)

}

data class ReviewerDetailsDTO(
        override val id: Long,
        override val address:String,
        override val name: String,
        override val username: String,
        override val email: String,
        override val phoneNumber: String,
        val course: String,
        val position: String,
        val evaluationPanelIds: List<Long>

) : UserDetailsDTO(id, address, name, username, email, phoneNumber, Roles.ROLE_REVIEWER)
{
    constructor(it: ReviewerDAO): this(it.id, it.address, it.name, it.username, it.email,
            it.phoneNumber, it.course, it.position, it.evaluationPanels.map { it.id })
}