package pt.unl.fct.iadi.project.api.StudentCV

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.project.config.SwaggerConfiguration
import pt.unl.fct.iadi.project.services.DTOs.dataItems.QuestionFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.QuestionFieldDetailsDTO


@RequestMapping("/studentsCVs")
interface StudentCVAPI {

    @ApiOperation(value = "Get the list of all CV items that are currently in the system")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of all CV items in the system"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @GetMapping("")
    fun getAllItems(): List<QuestionFieldDetailsDTO>


    @ApiOperation(value = "Add a CV item to the list of CV items of the system")
    @ApiResponses(
            ApiResponse(code = 201, message = "Successfully created a new CV item"),
            ApiResponse(code = 401, message = SwaggerConfiguration.RESPONSE_UNAUTHORIZED),
            ApiResponse(code = 403, message = SwaggerConfiguration.RESPONSE_FORBIDDEN),
            ApiResponse(code = 404, message = SwaggerConfiguration.RESPONSE_NOT_FOUND)
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    fun addOneItem(@RequestBody item: QuestionFieldDTO)

}