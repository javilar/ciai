package pt.unl.fct.iadi.project.services.DAO

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class InstitutionDAO(

        @Id
        @GeneratedValue
        var id: Long = 0,
        var name: String = "",
        var phoneNumber: String = ""

)