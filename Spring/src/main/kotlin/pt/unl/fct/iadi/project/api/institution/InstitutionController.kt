package pt.unl.fct.iadi.project.api.institution

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DTOs.InstitutionDTO
import pt.unl.fct.iadi.project.services.InstitutionService

@RestController
class InstitutionController(val institutions: InstitutionService) : InstitutionAPI {

   override fun getAll(): List<InstitutionDTO> = institutions.getAll().map { InstitutionDTO(it) }

   override fun getOne(id: Long): InstitutionDTO = InstitutionDTO(institutions.getOne(id))

}