package pt.unl.fct.iadi.project.api.reviewLogic.review

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.project.services.DTOs.ReviewDTO
import pt.unl.fct.iadi.project.services.ReviewService

@RestController
class ReviewController(val reviews: ReviewService) : ReviewAPI {

    override fun getOne(id: Long): ReviewDTO = ReviewDTO(reviews.getOne(id))

}