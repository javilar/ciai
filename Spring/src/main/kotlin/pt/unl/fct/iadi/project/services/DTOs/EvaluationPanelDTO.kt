package pt.unl.fct.iadi.project.services.DTOs

import pt.unl.fct.iadi.project.services.DAO.EvaluationPanelDAO
import pt.unl.fct.iadi.project.services.DAO.GrantCallDAO
import pt.unl.fct.iadi.project.services.DAO.ReviewerDAO
import pt.unl.fct.iadi.project.services.DTOs.users.ReviewerDetailsDTO

data class EvaluationPanelDTO(val reviewerEmails: List<String>,
                              val panelChairEmail: String)

{
    fun toDAO(reviewers: List<ReviewerDAO>, panelChair: ReviewerDAO, call: GrantCallDAO): EvaluationPanelDAO
            = EvaluationPanelDAO(0, reviewers, panelChair, call)
}

data class EvaluationPanelDetailsDTO(val id: Long,
                                     val reviewers: List<ReviewerDetailsDTO>,
                                     val panelChair: ReviewerDetailsDTO)

{
    constructor(it: EvaluationPanelDAO) : this(it.id, it.reviewers.map { ReviewerDetailsDTO(it) }, ReviewerDetailsDTO(it.panelChair) )
}

