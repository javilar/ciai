package pt.unl.fct.iadi.project.services.DTOs

import pt.unl.fct.iadi.project.services.DAO.GrantApplicationDAO
import pt.unl.fct.iadi.project.services.DAO.GrantCallDAO
import pt.unl.fct.iadi.project.services.DAO.StudentDAO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.AnswerFieldDTO
import pt.unl.fct.iadi.project.services.DTOs.dataItems.AnswerFieldDetailsDTO
import pt.unl.fct.iadi.project.services.DTOs.users.StudentDetailsDTO
import java.util.*

data class GrantApplicationDTO(
        val submissionDate: Date,
        val status: Int,
        val answers: List<AnswerFieldDTO>,
        val grantCallId: Long,
        val grantCallTitle: String
)

{
    constructor(it: GrantApplicationDAO) : this (it.submissionDate, it.status, it.answers.map { AnswerFieldDTO(it) }, it.grantCall.id, it.grantCall.title)

    fun toDAO(student: StudentDAO, call: GrantCallDAO): GrantApplicationDAO
            = GrantApplicationDAO(0, submissionDate, status, student, call, answers.map { it.toDAO(call.getQuestionByID(it.questionID)!!) }.toMutableList())

    fun updateDAO(applicationToUpdate: GrantApplicationDAO): GrantApplicationDAO {
        applicationToUpdate.submissionDate = this.submissionDate
        applicationToUpdate.status = this.status
        applicationToUpdate.answers = this.answers.map {
            var q = it.questionID
            var l = applicationToUpdate.grantCall.getQuestionByID(q)!!
            it.toDAO(l)
        }.toMutableList()

        return applicationToUpdate
    }

}

data class GrantApplicationDetailsDTO(val id: Long,
                                      val submissionDate: Date,
                                      val status: Int,
                                      val answers: List<AnswerFieldDetailsDTO>,
                                      val grantCallId: Long,
                                      val grantCallTitle: String,
                                      val studentDetails: StudentDetailsDTO
)

{
    constructor(it: GrantApplicationDAO) : this (it.id, it.submissionDate, it.status, it.answers.map { AnswerFieldDetailsDTO(it) }, it.grantCall.id, it.grantCall.title, StudentDetailsDTO(it.student))
}
/*
data class StudentApplicationSummaryDTO(
        var submissionDate: Date = Date(),
        var status: Int = 0
)*/
