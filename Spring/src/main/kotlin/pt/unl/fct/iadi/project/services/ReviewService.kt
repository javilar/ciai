package pt.unl.fct.iadi.project.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.ReviewRepository
import pt.unl.fct.iadi.project.services.DAO.NotFoundException
import pt.unl.fct.iadi.project.services.DAO.ReviewDAO

@Service
class ReviewService(val reviews: ReviewRepository) {

    fun getOne(id: Long): ReviewDAO =
            reviews.findById(id).orElseThrow { NotFoundException("Review with id $id does not exist") }

}