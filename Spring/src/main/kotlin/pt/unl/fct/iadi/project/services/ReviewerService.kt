package pt.unl.fct.iadi.project.services

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.GrantApplicationRepository
import pt.unl.fct.iadi.project.model.ReviewerRepository
import pt.unl.fct.iadi.project.model.UserRepository
import pt.unl.fct.iadi.project.services.DAO.*


@Service
class ReviewerService(val reviewers: ReviewerRepository,
                      val users: UserRepository,
                      val applications: GrantApplicationRepository
) {

    fun getOne(username: String): ReviewerDAO =  reviewers.findById(username).orElseThrow {
        NotFoundException("Reviewer with username $username not found")
    }

    fun getReviewerReviews(username: String): List<ReviewDAO> {
        val reviewer = reviewers.findByIdWithReviews(username).orElseThrow {
            NotFoundException("Reviewer with username $username not found")
        }

        return reviewer.reviews
    }

    fun create(reviewer: ReviewerDAO){
        val reviewerUsername = reviewer.username
        users.findById(reviewerUsername).ifPresent {
            throw AlreadyExistsException("A user with username $reviewerUsername already exists")
        }
        reviewer.password = BCryptPasswordEncoder().encode(reviewer.password)
        reviewers.save(reviewer)
    }


    fun getReviewerApplications(username: String): Iterable<GrantApplicationDAO> {
        val reviewer = reviewers.findById(username).orElseThrow {
            NotFoundException("Reviewer with username $username not found")
        }

        return applications.findReviewerApplications(username)
    }

    fun getPanelChairApplications(username: String): Iterable<GrantApplicationDAO> {
        val reviewer = reviewers.findById(username).orElseThrow {
            NotFoundException("Reviewer with username $username not found")
        }

        return applications.findPanelChairApplications(username)
    }

}