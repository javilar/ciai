package pt.unl.fct.iadi.project.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.project.model.EvaluationPanelRepository
import pt.unl.fct.iadi.project.model.GrantApplicationRepository
import pt.unl.fct.iadi.project.model.GrantCallRepository
import pt.unl.fct.iadi.project.services.DAO.*
import java.util.*

@Service
class GrantCallService(var calls: GrantCallRepository,
                       var evalPanels: EvaluationPanelRepository,
                       var applications: GrantApplicationRepository
) {

    fun getAll(): Iterable<GrantCallDAO> = calls.findAll()

    fun getAllOpen(): Iterable<GrantCallDAO> = calls.findAllByOpeningDateBeforeAndClosingDateAfter(Date(), Date())

    fun createGrantCall(call: GrantCallDAO){
        calls.findById(call.id).ifPresent {
            throw AlreadyExistsException("Grant Call with id ${call.id} already exists")
        }
        calls.save(call)
    }

    fun getOne(id: Long): GrantCallDAO =
        calls.findById(id).orElseThrow { NotFoundException("Grant Application with id $id does not exist") }

    fun updateGrantCall(call: GrantCallDAO){
        calls.findById(call.id).orElseThrow {
            NotFoundException("Grant Call with id ${call.id} not found")
        }
        calls.save(call)
    }

    fun deleteGrantCall(id: Long){
        calls.findById(id).orElseThrow {
            NotFoundException("Grant Call with id $id not found")
        }
        calls.deleteById(id)
    }

    fun getGrantCallApplicationsByCallId(id: Long): List<GrantApplicationDAO> {
        val call = calls.findByIdWithApplications(id).orElseThrow {
            NotFoundException("Grant Call with id $id not found")
        }

        return call.applications
    }

    fun getAcceptedGrantCallApplicationsByCallId(id: Long): Iterable<GrantApplicationDAO> {
        val call = calls.findByIdWithApplications(id).orElseThrow {
            NotFoundException("Grant Call with id $id not found")
        }

        var acceptedApplications = applications.findAcceptedApplicationsByCallId(id)

        return acceptedApplications
    }


    fun defineEvaluationPanel(id: Long, evaluationPanel: EvaluationPanelDAO) {
        val call = calls.findById(id).orElseThrow {
            NotFoundException("Grant Call with id $id not found")
        }

        if(call.evaluationPanel != null)
            throw AlreadyExistsException("Grant Call already has an evaluation panel")

        if(!evaluationPanel.reviewers.contains(evaluationPanel.panelChair)) {
            var reviewerListWithPanelChair = evaluationPanel.reviewers.toMutableList()
            reviewerListWithPanelChair.add(evaluationPanel.panelChair)
            evaluationPanel.reviewers = reviewerListWithPanelChair
        }

        evalPanels.save(evaluationPanel)
    }


    fun getEvaluationPanel(id: Long): EvaluationPanelDAO  {
        val call = calls.findByIdWithEvaluationPanel(id).orElseThrow {
            NotFoundException("Grant Call with id $id not found")
        }

        if(call.evaluationPanel == null)
            throw NotFoundException("Grant Call with id $id does not have an evaluation panel assigned")

        return call.evaluationPanel!!
    }

}

