import {createStyles, withStyles, WithStyles} from '@material-ui/core/styles';
import React, {memo, useEffect} from 'react';
import {
    GrantApplicationDetailsDTO,
    GrantCallDetailsDTO,
    QuestionFieldDetailsDTO,
    StudentsApi
} from "../api";
import Typography from "@material-ui/core/Typography";
import Appbar from "../Homepage/Appbar";
import {Button} from "@material-ui/core";

import {Link, match, Redirect} from "react-router-dom";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import FundedApplicationElement from "./FundedApplicationElement";
import {GrantCallsApi} from "../api"
import {getAuthorizationHeader} from "../DataManipulation";

const styles = createStyles({
    root: {
        marginLeft: 30,
        marginRight: 30,
        marginTop: 20,
        minWidth: 275,
        borderRadius: 10,
        position: 'relative',
        paddingBottom: 50
    },
    h4: {
        marginLeft: 5,
    },
    formControl: {
        margin: '10px',
        minWidth: 150,
        marginLeft: 'auto',
        marginRight: 35
    },
    rowC: {
        marginTop: 10,
        display:'flex',
        flexDirection:'row',
    },
    applyToGrantButton: {
        position: 'absolute',
        right: 20,
        bottom: 10,

    },
    ul: {
        fontSize: 0
    },
    fundedApplicationsTitle: {
        marginTop: 50,
        marginBottom: 0,
        margin: 35
    },
});

type TParams = { id: string };

type Props = {
    isSignedIn: boolean,
    signIn:(b:boolean)=>void,
    match: match<TParams>,
    grantCall?: GrantCallDetailsDTO
};

export type CaptionedValueProps = Props & WithStyles<typeof styles>;

const GrantCallDetails: React.FC<CaptionedValueProps> =
    ({classes, isSignedIn, signIn, match, grantCall}) => {

    const [call, setCall] = React.useState<GrantCallDetailsDTO>();
    const [userRole, setUserRole] = React.useState<string>();
    const [currentApplicationId, setCurrentApplicationId] = React.useState<number>(-1);
    const [isClosed, setIsClosed] = React.useState<boolean>(false);
    const [redirectToHome, setRedirectToHome] = React.useState<boolean>(false);
    const [fundedApplications, setFundedApplications] = React.useState<Array<GrantApplicationDetailsDTO>>([] as Array<GrantApplicationDetailsDTO>);

    useEffect(() => {
        if(!grantCall) {    // if the grant call is not passed via props, fetch it from the server
            new GrantCallsApi().getOneCall(+match.params.id, getAuthorizationHeader())
                .then(call => setCall(call))
                .catch(() => setRedirectToHome(true))
        }

        const role: string | null = localStorage.getItem('role')
        if( role !== null ) setUserRole(role)

        if (role === 'ROLE_STUDENT') {
            const username = localStorage.getItem('username')
            if(username != null) {
                new StudentsApi().getStudentApplicationsUsingGET(username, getAuthorizationHeader())
                    .then(applications => {
                        const currentApplication = applications.find(element =>
                            element.grantCallId === +match.params.id)

                        if(currentApplication !== undefined)
                            setCurrentApplicationId(currentApplication.id)
                })
            }
        }

        const closedCondition = !redirectToHome && grantCall && Date.parse(grantCall.closingDate.toString()).toString() < Date.now().toString();

        if(closedCondition) {
            new GrantCallsApi().getGrantCallApplications(+match.params.id, getAuthorizationHeader())
                .then(acceptedApplications => {
                    setFundedApplications(acceptedApplications)
            })
        }

        setIsClosed(closedCondition || false)

    }, [match.params.id, call, grantCall, setFundedApplications, setCall]);

    const typedCall = grantCall ? grantCall : call as GrantCallDetailsDTO

    const isButtonDisabled = isClosed || !isSignedIn || userRole !== 'ROLE_STUDENT' || currentApplicationId !== -1

    return (
        <>
            {redirectToHome ? <Redirect to="/" /> :
                <>
                    <Appbar isSignedIn={isSignedIn} signIn={signIn}/>
                    <Card elevation={4} className={classes.root}>
                        <CardContent>
                            <Typography variant="h6" component="h2" style={{marginBottom: 25}}>
                                {typedCall && typedCall.title}
                            </Typography>

                            <div className={classes.rowC}>
                                <Typography variant="body2" component="div" className={classes.h4}>
                                    <b>Description:</b>
                                </Typography>

                                <Typography variant="body2" component="div" className={classes.h4}>
                                    {typedCall && typedCall.description}
                                </Typography>
                            </div>

                            <div className={classes.rowC}>
                                <Typography variant="body2" component="div" className={classes.h4}>
                                    <b>Requirements:</b>
                                </Typography>
                                <ul className={classes.ul}>
                                    {typedCall && typedCall.requirements.map((r: String, index: number) =>
                                        <Typography key={index} variant="body2" component="div" className={classes.h4}>
                                            <li>
                                                {r}
                                            </li>
                                        </Typography>
                                    )}
                                </ul>
                            </div>

                            <div className={classes.rowC}>
                                <Typography variant="body2" component="div" className={classes.h4}>
                                    <b>Funding:</b>
                                </Typography>

                                <Typography variant="body2" component="div" className={classes.h4}>
                                    {typedCall && typedCall.funding}
                                </Typography>
                            </div>

                            <div className={classes.rowC}>
                                <Typography variant="body2" component="div" className={classes.h4}>
                                    <b>OpeningDate:</b>
                                </Typography>

                                <Typography variant="body2" component="div" className={classes.h4}>
                                    {typedCall && typedCall.openingDate}
                                </Typography>
                            </div>

                            <div className={classes.rowC}>
                                <Typography variant="body2" component="div" className={classes.h4}>
                                    <b>ClosingDate:</b>
                                </Typography>

                                <Typography variant="body2" component="div" className={classes.h4}>
                                    {typedCall && typedCall.closingDate}
                                </Typography>
                            </div>

                            <div className={classes.rowC}>
                                <Typography variant="body2" component="div" className={classes.h4}>
                                    <b>Questions:</b>
                                </Typography>

                                <ul className={classes.ul}>
                                    {typedCall && typedCall.questionFields.map((question: QuestionFieldDetailsDTO) =>
                                        <Typography key={question.id} variant="body2" component="div" className={classes.h4}>
                                            <li>
                                                {question.title}
                                            </li>
                                        </Typography>
                                    )}
                                </ul>

                            </div>

                            <div className={classes.rowC}>
                                <Typography variant="body2" component="div" className={classes.h4}>
                                    <b>Number of submitted applications:</b>
                                </Typography>

                                <Typography variant="body2" component="div" className={classes.h4}>
                                    {typedCall && typedCall.numberOfApplications}
                                </Typography>
                            </div>

                            {
                                isButtonDisabled ?

                                    (currentApplicationId !== -1) ?
                                        <Button className={classes.applyToGrantButton} variant="contained"
                                                component={Link} to={`/applications/${currentApplicationId}`}>
                                            See application
                                        </Button>
                                        :
                                        <Button className={classes.applyToGrantButton} variant="contained"
                                                disabled = {true} >Apply to Grant Call</Button>
                                    :
                                    <Link to={{
                                        pathname: `/grantCalls/${typedCall && typedCall.id}/apply`,
                                        state: {
                                            grantCall: typedCall
                                        }
                                    }}>
                                        <Button className={classes.applyToGrantButton} variant="contained">
                                            Apply to Grant Call
                                        </Button>
                                    </Link>
                            }

                        </CardContent>
                    </Card>

                    {
                        isClosed && fundedApplications.length > 0 &&
                            <>
                                <Typography variant="h5" component="h2" className={classes.fundedApplicationsTitle}>
                                    Funded applications
                                </Typography>

                                {fundedApplications.map((application, index) =>
                                    <FundedApplicationElement key={index} application={application}/>)}

                            </>
                    }

                </>
            }
        </>

    );
}

const GrantCallDetailsWithStyles = memo(withStyles(styles)(GrantCallDetails));
export default GrantCallDetailsWithStyles;