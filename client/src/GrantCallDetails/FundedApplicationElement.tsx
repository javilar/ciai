import React from 'react';
import {createStyles, makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {GrantApplicationDetailsDTO} from "../api";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() =>
    createStyles({

        cardContent: {
            marginLeft: 30,
            marginRight: 30,
            marginTop: 20,
            marginBottom: 20,
            minWidth: 275,
            borderRadius: 10,
        },
    }),
);

export default function FundedApplicationElement({application}:{application: GrantApplicationDetailsDTO}) {
    const classes = useStyles();

    return (
        <Link to={{
            pathname: `/applications/${application.id}`,
            state: {
                grantApplication: application
            }
        }} style={{ textDecoration: 'none' }}>
            <Card elevation={3} className={classes.cardContent} color={"primary"} >
                <CardContent>
                    <Typography variant="body2" component="div" >
                        <ul style={{margin:0}}>
                            <li> <b> Student username: </b> {application.studentDetails.name} </li>
                            <li> <b> Student number: </b> {application.studentDetails.number} </li>
                            <li> <b> Submission date: </b> {application.submissionDate} </li>
                        </ul>
                    </Typography>
                </CardContent>
            </Card>
        </Link>
    );
}

