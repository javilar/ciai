import React, {memo, useEffect} from 'react';
import '../App.css';
import {
    Button,
    createStyles,
    withStyles,
    WithStyles
} from "@material-ui/core";
import Appbar from "../Homepage/Appbar";
import {
    match, Redirect
} from "react-router-dom";
import {
    AnswerFieldDTO,
    GrantApplicationDTO,
    GrantCallDetailsDTO, GrantCallsApi,
    StudentsApi
} from "../api";
import ApplicationQuestionListElement from "./ApplicationQuestionListElement";
import SaveIcon from '@material-ui/icons/Save';
import Typography from "@material-ui/core/Typography";
import {getAuthorizationHeader} from "../DataManipulation";

const styles = createStyles({

    root: {
      paddingBottom: 100
    },
    button:{
        marginTop: '10px',
        marginRight: '150px',

    },

    upload: {
        '& > *': {
            margin:'0px',
        },
    },

    submitNowButton: {
        position: 'absolute',
        right: 30,
        marginTop: '20px'

    },
    saveNowButton: {
        position: 'absolute',
        right: 170,
        marginTop: '20px',
    },
    title: {
        margin: 35
    }

});

type TParams = { id: string };

type Props = {
    isSignedIn: boolean,
    signIn:(b:boolean)=>void,
    match: match<TParams>,
    grantCall?: GrantCallDetailsDTO
};


export type CaptionedValueProps = Props & WithStyles<typeof styles>;

const GrantApplicationForm: React.FC<CaptionedValueProps> =
    ({classes, isSignedIn, signIn, match, grantCall}) => {

    const [call, setCall] = React.useState<GrantCallDetailsDTO>();
    const [answers, setAnswers] = React.useState<Array<AnswerFieldDTO>>([] as Array<AnswerFieldDTO>);
    const [redirectToProfile, setRedirectToProfile] = React.useState<boolean>(false);
    const [redirectToHome, setRedirectToHome] = React.useState<boolean>(false);

    const onButtonClick = (callStatus: number) => {
        const callDTO: GrantCallDetailsDTO = call as GrantCallDetailsDTO
        const username: string | null = localStorage.getItem('username')

        if(callDTO && username != null) {

            let mandatoryEmpty: boolean = false;
            let a = answers.slice()
            callDTO.questionFields.map((q, index) => {

                const checkEmptyAnswer = !answers[index]
                    || (answers[index] && (answers[index].dataItem == undefined
                        || answers[index].dataItem === ''))

                if(checkEmptyAnswer) {
                    a[index] = {
                        dataItem: '',
                        questionID: q.id
                    }
                }

                if(q.mandatory && checkEmptyAnswer)
                    mandatoryEmpty = true;

            })

            setAnswers(a)

            if(mandatoryEmpty) {
                alert("Please answer all mandatory questions")
            }
            else {
                const date: Date = new Date()
                const newApplication: GrantApplicationDTO = {
                    submissionDate: date,
                    status: callStatus,
                    answers: a,
                    grantCallId: callDTO.id,
                    grantCallTitle: callDTO.title
                }

                new StudentsApi().createApplication(newApplication, username, getAuthorizationHeader())
                    .then(() => setRedirectToProfile(true))
                    .catch(reason => alert(reason.statusText + " - You cannot apply to this call, because you already have an application to it."))
            }

        }
    }

    const buildAnswerArray = (childIndex: number, answerDTO: AnswerFieldDTO) => {
        let a = answers.slice()
        a[childIndex] = answerDTO

        setAnswers(a)
    }


    useEffect(() => {

        if(!grantCall)
            new GrantCallsApi().getOneCall(+match.params.id, getAuthorizationHeader())
                .then(call => setCall(call))
                .catch(() => setRedirectToHome(true))

        else
            setCall(grantCall)

    }, [grantCall, match.params.id]);

    const role: string | null = localStorage.getItem('role')
    const typedCall = grantCall ? grantCall : call as GrantCallDetailsDTO
    const isClosed: boolean = typedCall && (Date.parse(typedCall.openingDate.toString()).toString() > Date.now().toString() || Date.parse(typedCall.closingDate.toString()).toString() < Date.now().toString());
    const redirectToHomepage: boolean = redirectToHome || ((role !== undefined && role !== 'ROLE_STUDENT') || isClosed)

    return (
        <>
            {redirectToHomepage ? <Redirect to="/"/> :
                redirectToProfile ? <Redirect to="/profile"/> :
                <div className={classes.root}>
                    <Appbar isSignedIn={isSignedIn} signIn={signIn}/>

                    <Typography variant="h5" component="h2" className={classes.title}>
                        Application for {typedCall && typedCall.title}
                    </Typography>

                    {typedCall && typedCall.questionFields.map((question, index) =>
                        <ApplicationQuestionListElement
                            key={index} question={question} index={index}
                            buildAnswerArray={buildAnswerArray}
                        />)}

                    <Button className={classes.submitNowButton} variant="contained"
                            onClick={() => onButtonClick(1)}>
                        Submit now
                    </Button>

                    <Button className={classes.saveNowButton} variant="contained"
                            onClick={() => onButtonClick(0)} startIcon={<SaveIcon />}>
                        Save
                    </Button>
                </div>
            }
        </>
    );
}



const GrantApplicationFormWithStyles = memo(withStyles(styles)(GrantApplicationForm));
export default GrantApplicationFormWithStyles;
