import React, {memo} from 'react';
import {createStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {AnswerFieldDTO, QuestionFieldDetailsDTO} from "../api";
import {
    FormControl,
    FormControlLabel,
    FormLabel, Grid,
    Radio,
    RadioGroup,
    TextField,
    withStyles,
    WithStyles
} from "@material-ui/core";



const styles =
    createStyles({
        cardContent: {
            marginLeft: 30,
            marginRight: 30,
            marginTop: 20,
            minWidth: 275,
            borderRadius: 10,
            position: 'relative',
        },
        upload: {
            '& > *': {
                marginLeft:'0px',
                marginRight: '10px'
            },
        },
        input: {
            display: 'none',
        },
        h4: {
            marginLeft: 5,
            marginBottom: 20
        },
        mandatorySignal: {
            marginLeft: 15,
            color: 'red'
        }
    },
);


type Props = {
    question: QuestionFieldDetailsDTO,
    index: number,
    buildAnswerArray?: (childIndex: number, answerDTO: AnswerFieldDTO) => void,
    existingAnswer?: string,
    submitted?: boolean
};

export type CaptionedValueProps = Props & WithStyles<typeof styles>;

const ApplicationQuestionListElement: React.FC<CaptionedValueProps> =
    ({classes, question, index, buildAnswerArray, existingAnswer, submitted}) => {

    const [answer, setAnswer] = React.useState<string>(existingAnswer||'');

    const handleChange = (answer: string|undefined) => {
        if(answer !== undefined) {
            const answerDTO: AnswerFieldDTO = {
                dataItem: answer,
                questionID: question.id
            }

            if (buildAnswerArray) {
                buildAnswerArray(index, answerDTO)
            }

            setAnswer(answer)
        }

    }

    const mandatoryQuestionSignal = question.mandatory ?
            <Typography variant="h6" component="h2" className={classes.mandatorySignal} align={"justify"}>
                *
            </Typography>
        : <></>

    return (
        <Card elevation={3} className={classes.cardContent} color={"primary"}>
            <CardContent>
                <Grid container>
                    <Typography variant="h6" component="h2" style={{marginLeft: 5}} align={"left"}>
                            {question.title}
                        </Typography>

                    {mandatoryQuestionSignal}
                </Grid>

                <Typography variant="body2" component="div" className={classes.h4}>
                    {question.description}
                </Typography>


                {
                question.datatype === 'Choice'?

                    <div>
                        <FormControl component="fieldset" disabled={submitted}>
                            <FormLabel component="legend">Choose option</FormLabel>
                            <RadioGroup aria-label="Choose" name="gender1" value={answer}
                                        onChange={(e) => handleChange(e.target.value)}>
                                <FormControlLabel value="yes" control={<Radio />} label="Yes" />
                                <FormControlLabel value="no" control={<Radio />} label="No" />
                            </RadioGroup>
                        </FormControl>
                    </div>

                    : question.datatype === 'Text'?

                        <TextField
                            id="outlined-full-width"
                            style={{ marginRight: 20 }}
                            placeholder="Answer"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            disabled={submitted}
                            defaultValue={answer}
                            onChange={(e) => handleChange(e.target.value)}
                        />

                        :

                        <div className={classes.upload}>
                            <input
                                accept="/*"
                                className={classes.input}
                                id="contained-button-file"
                                multiple
                                type="file"
                                disabled={submitted}
                                onChange={(e) => {
                                    if(e.target.files)
                                        handleChange(e.target.files[0].name)
                                }}
                            />
                            <label htmlFor="contained-button-file">
                                <Button variant="contained" color="primary" component="span" disabled={submitted}>
                                    Upload
                                </Button>
                            </label>
                            {answer}
                        </div>


                }

            </CardContent>
        </Card>
    );
}

const ApplicationQuestionListElementWithStyles = memo(withStyles(styles)(ApplicationQuestionListElement));
export default ApplicationQuestionListElementWithStyles;


