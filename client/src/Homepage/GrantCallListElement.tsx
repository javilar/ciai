import React from 'react';
import {createStyles, makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {GrantCallDetailsDTO} from "../api";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            marginLeft: 30,
            marginRight: 30,
            marginTop: 20,
            minWidth: 275,
            borderRadius: 10
        },
        bullet: {
            display: 'inline-block',
            margin: '0 2px',
            transform: 'scale(0.8)',
        },
        title: {
            fontSize: 14,
        },
        pos: {
            marginTop: 12,
            marginBottom: 8,
        },
        rowC: {
            position: 'relative',
            display:'flex',
            flexDirection:'row',
        },
        datesSection: {
            position: 'absolute',
            top: '70%',
            right: 0,
            marginRight: 10,
            textAlign: 'center',
        },
        dateElement: {
            marginBottom: 3,
            textAlign: 'left'
        },
        open: {
            color: 'green',
            marginTop: 0,
            marginBottom: 15,
            borderBottom: '2px solid gray',
            paddingBottom: '10px'
        },
        closed: {
            color: 'red',
            marginTop: 0,
            marginBottom: 15,
            borderBottom: '2px solid gray',
            paddingBottom: '10px'
        },
        button: {
            marginLeft: 2
        },
    }),
);

export default function CallListElement(props:{call: GrantCallDetailsDTO}) {
    const classes = useStyles();

    const isOpen = Date.parse(props.call.openingDate.toString()).toString() <= Date.now().toString()
        && Date.parse(props.call.closingDate.toString()).toString() > Date.now().toString();

    return (
        <Card elevation={3} className={classes.root} color={"primary"}>
            <CardContent>
                <div className={classes.rowC}>
                    <Typography variant="h6" component="h2">
                        {props.call.title}
                    </Typography>

                    <div className={classes.datesSection}>
                        {
                            isOpen ?
                                <h4 className={classes.open}>Open</h4> :
                                <h4 className={classes.closed}>Closed</h4>
                        }
                        <Typography variant="body2" component="p" className={classes.dateElement}>
                            <b>Opening date:</b> {props.call.openingDate}
                        </Typography>
                        <Typography variant="body2" component="p" className={classes.dateElement}>
                            <b>Closing date:</b> {props.call.closingDate}
                        </Typography>
                    </div>
                </div>

                <Typography className={classes.pos} color="textSecondary">
                    Description:
                </Typography>
                <Typography variant="body2" component="p" >
                    {props.call.description}
                </Typography>

            </CardContent>

            <CardActions className={classes.button}>
                <Link to={{
                    pathname: `/grantCalls/${props.call.id}`,
                    state: {
                        grantCall: props.call
                    }
                }}>
                    <Button size="small">Learn More</Button>
                </Link>
            </CardActions>

        </Card>
    );
}

