import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Link} from "react-router-dom";
import {IconButton} from "@material-ui/core";
import {AccountCircle} from "@material-ui/icons";
import {performLogout} from "../DataManipulation";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 3,
            fontWeight: 'bold',
            textDecoration: 'none',
            color: 'inherit',

            '&:focus, &:hover, &:visited, &:link, &:active': {
                textDecoration: 'none',
            }
        },
    }),
);

export default function Appbar(props:{isSignedIn: boolean, signIn:(b:boolean)=>void}) {
    const classes = useStyles();

    const onLogOutClick = () => {
        performLogout(props.signIn);
    }

    const username = localStorage.getItem('username') || ""

    return (

            <div className={classes.root}>
                <AppBar position="static" style={{background: '#1489b6'}}>
                    <Toolbar>
                        <Typography variant="h5" className={classes.title} component={Link} to={"/"}>
                            Grant Management System
                        </Typography>
                        {props.isSignedIn ?
                            <>
                                <Typography variant="body2">
                                    {username}
                                </Typography>
                                <IconButton
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    color="inherit"
                                    component={Link}
                                    to={"/profile"}
                                >
                                    <AccountCircle />
                                </IconButton>

                                <Button color="inherit" onClick={onLogOutClick} component={Link} to={"/"}>Sign out</Button>
                            </>
                            :

                            <Button color="inherit" component={Link} to={"/login"} >Sign in</Button>
                        }

                    </Toolbar>
                </AppBar>
            </div>

    );
}

