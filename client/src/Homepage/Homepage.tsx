import {createStyles, withStyles, WithStyles} from '@material-ui/core/styles';
import React, {memo, useEffect} from 'react';
import Appbar from "./Appbar";
import GrantCallListElement from './GrantCallListElement'
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {GrantCallDetailsDTO} from "../api";
import Typography from "@material-ui/core/Typography";
import {GrantCallsApi} from "../api"
import {getAuthorizationHeader} from "../DataManipulation";

const styles = createStyles({
    root: {
        marginBottom: '30px'
    },
    h4: {
        margin: 35
    },
    formControl: {
        margin: '10px',
        minWidth: 150,
        marginLeft: 'auto',
        marginRight: 35
    },
    rowC: {
        marginTop: 10,
        display:'flex',
        flexDirection:'row',
    }
});

type Props = {
    isSignedIn: boolean,
    signIn:(b:boolean)=>void
};

export type CaptionedValueProps = Props & WithStyles<typeof styles>;

const Homepage: React.FC<CaptionedValueProps> = ({classes, isSignedIn, signIn}) => {

    const [ filter, setFilter ] = React.useState("");
    const [ calls, setCalls ] = React.useState([] as GrantCallDetailsDTO[]);

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setFilter(event.target.value as string);
    };

    useEffect(() => {
        new GrantCallsApi().getAllCalls(getAuthorizationHeader()).then(calls => setCalls(calls))
    }, [setCalls]);

    let filteredList = filter === 'open' ? calls.filter(call => Date.parse(call.closingDate.toString()).toString() >= Date.now().toString())
        : calls


    return (
        <div className={classes.root}>
            <Appbar isSignedIn={isSignedIn} signIn={signIn}/>
            <div className={classes.rowC}>
                <Typography variant="h5" component="h2" className={classes.h4}>
                    Available Grant Calls:
                </Typography>

                <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel htmlFor="outlined-age-native-simple">Filter</InputLabel>
                    <Select
                        native
                        value={filter}
                        onChange={handleChange}
                        label="Filter"
                        inputProps={{
                            name: 'age',
                            id: 'outlined-age-native-simple',
                        }}
                    >
                        <option value={'all'}>All</option>
                        <option value={'open'}>Open</option>
                    </Select>
                </FormControl>

            </div>

            {filteredList.map((c: GrantCallDetailsDTO) =>
            {
            return c &&
                <GrantCallListElement key={c.id} call={c}/>;
            })}
        </div>
    );
}

const HomepageWithStyles = memo(withStyles(styles)(Homepage));
export default HomepageWithStyles;