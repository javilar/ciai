import React, {useEffect, useState} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import LoginWithStyles from "./Login/LoginForm";
import HomepageWithStyles from "./Homepage/Homepage";
import GrantCallDetailsWithStyles from "./GrantCallDetails/GrantCallDetails";
import GrantApplicationFormWithStyles from "./GrantApplicationForm/GrantApplicationForm";
import ProfilePageWithStyles from "./ProfilePage/ProfilePage";
import {performLogout} from "./DataManipulation";
import ApplicationDetailsWithStyles from "./Applications/ApplicationDetails";
import EvaluationFormWithStyles from "./EvaluationForm/EvaluationForm";
import {Configuration} from "./api";


const App: React.FC = () => {

    const [signedIn, setSignedIn] = useState(false);

    useEffect(() => {
        const jwtToken: string | null = localStorage.getItem('jwt')

        if(jwtToken) {
            // and test if it is expired, if yes clean it up
            const token = JSON.parse(atob(jwtToken.split('.')[1]))

            console.log(token)

            if (Date.now() >= token.exp * 1000) {
                console.log('LOGOUT BECAUSE TOKEN EXPIRED')
                performLogout(setSignedIn)
            }

            else
                setSignedIn(true)

        }
    }, []);


    return (
          <Router>
            <Switch>
                <Route path="/login">
                  <LoginWithStyles isSignedIn={signedIn} signIn={setSignedIn}/>
                </Route>
                <Route path="/applications/:id/review" render={props => <EvaluationFormWithStyles {...props} isSignedIn={signedIn} signIn={setSignedIn}
                                                                                                  grantApplication={// @ts-ignore
                                                                                                      props.location.state?.grantApplication}/>} />
                <Route path="/applications/:id/finalEvaluation" render={props => <EvaluationFormWithStyles {...props} isSignedIn={signedIn}
                                                                                                           signIn={setSignedIn} isFinalEvaluation={true}
                                                                                                           grantApplication={// @ts-ignore
                                                                                                               props.location.state?.grantApplication}/>} />

                <Route path="/applications/:id" render={props => <ApplicationDetailsWithStyles {...props} isSignedIn={signedIn} signIn={setSignedIn}
                                                                                               reviewed={// @ts-ignore
                                                                                                   props.location.state?.reviewed}
                                                                                               grantApplication={// @ts-ignore
                                                                                                   props.location.state?.grantApplication}/>} />

                <Route path="/grantCalls/:id/apply" render={props => <GrantApplicationFormWithStyles {...props} isSignedIn={signedIn} signIn={setSignedIn}
                                                                                                     grantCall={// @ts-ignore
                                                                                                            props.location.state?.grantCall}/>} />


                <Route path="/grantCalls/:id" render={props => <GrantCallDetailsWithStyles {...props} isSignedIn={signedIn} signIn={setSignedIn}
                                                                                           grantCall={// @ts-ignore
                                                                                                    props.location.state?.grantCall}/>} />
                <Route path="/profile" render={props => <ProfilePageWithStyles {...props} isSignedIn={signedIn} signIn={setSignedIn}/>} />
                <Route path="/">
                    <HomepageWithStyles isSignedIn={signedIn} signIn={setSignedIn}/>
                </Route>
            </Switch>
          </Router>

      );
    }

export default App;
