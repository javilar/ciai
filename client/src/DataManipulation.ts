import {UserDetailsDTO} from "./api";

export async function performLogin(username: string, password: string, signIn: (b: boolean) => void) {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    fetch("/login", {
        mode: 'cors',
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify({username: username, password: password})
    })
        .then(response => {
            if (response.ok)
                return response.headers.get('Authorization');
            else {
                console.log(`Error: ${response.status}: ${response.statusText}`)
                alert("ERRADO!")
                return null;
                // and add a message to the Ui: wrong password ?? other errors?
            }
        })
        .catch(err => {
            console.log('This is an error: ' + err)
        })
        .then(token => {
            if (token) {
                const jwt = JSON.parse(atob(token.split('.')[1]))

                localStorage.setItem('jwt', token);

                const auth = {'Authorization': token};

                fetch(`/users/${jwt.username}`, {
                    mode: 'cors',
                    method: 'GET',
                    headers: {
                        ...auth,
                        'Content-Type': 'application/json'
                    }
                })
                    .then(response => {
                        if (response.ok) {
                            return response.json()
                        } else {
                            throw new Error(response.statusText)
                        }
                    })
                    .then(data => {

                        const user = data as unknown as UserDetailsDTO
                        if (user) {
                            localStorage.setItem('username', user.username);
                            localStorage.setItem('role', user.role.toString());
                            localStorage.setItem('name', user.name)
                        }

                    })
                    .catch(reason => {
                        console.log('ERROR:', reason)
                    });

                signIn(true)
            }
        })
}

export function performLogout(signIn: (b: boolean) => void) {
    signIn(false);
    localStorage.removeItem('jwt');
    localStorage.removeItem('role');
    localStorage.removeItem('username');
    localStorage.removeItem('name')
}

export function getAuthorizationHeader() {
    const jwtToken: string | null = localStorage.getItem('jwt')

    if(jwtToken != null)
        return {
            headers: {
                'Authorization': jwtToken
            }
        }

    return {}
}