import React, {memo, useState} from 'react';
import '../App.css';
import {Button, Card, createStyles, TextField, withStyles, WithStyles} from "@material-ui/core";
import Appbar from "../Homepage/Appbar";
import {Link, Redirect} from 'react-router-dom';
import {performLogin} from "../DataManipulation";


const styles = createStyles({
    root: {
        display: 'flex',
        justifyContent:'center',
        alignItems:'center',
        height: '50vh',
        maxWidth: 400,
        margin: '0 auto',
        float: 'none',
        marginTop: '50px',
        borderRadius: 10
    },
    button:{
        marginTop: '20px',
        marginRight: '150px',
    },
    passwordDiv:{
        marginTop: '20px',
    }
});


type Props = {
    isSignedIn: boolean,
    signIn:(b:boolean)=>void
};

export type CaptionedValueProps = Props & WithStyles<typeof styles>;

const LoginForm: React.FC<CaptionedValueProps> = ({classes, isSignedIn, signIn}) => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const onEmailChange = (e: any) => {
        setUsername(e.target.value);
    }

    const onPasswordChange = (e: any) => {
        setPassword(e.target.value);
    }

    const onButtonClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        e.preventDefault();
        performLogin(username, password, signIn);
    }

    const handleKeyDown = (event: { key: string; }) => {
        if (event.key === 'Enter') {
            performLogin(username, password, signIn);
        }
    }

    return (
        <>
        {isSignedIn ? (
            <Redirect to="/"/>
            ) :
                <>
                    <Appbar isSignedIn={isSignedIn} signIn={signIn}/>
                    <Card className={classes.root} elevation={3}>
                        <div>
                            <h2>Sign in</h2>
                            <div>
                                <TextField
                                    id="standard-email-input"
                                    label="Email"
                                    onChange={onEmailChange}
                                    onKeyPress={handleKeyDown}
                                />
                            </div>
                            <div>
                                <TextField className={classes.passwordDiv}
                                           id="standard-password-input"
                                           label="Password"
                                           type="password"
                                           autoComplete="current-password"
                                           onChange={onPasswordChange}
                                           onKeyPress={handleKeyDown}
                                />
                            </div>
                            <Button className={classes.button} onClick={onButtonClick} component={Link} to={"/"}>
                                Sign in
                            </Button>
                        </div>
                    </Card>
                </>
        }
        </>
    );
}



const LoginWithStyles = memo(withStyles(styles)(LoginForm));
export default LoginWithStyles;
