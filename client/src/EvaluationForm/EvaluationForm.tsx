import React, {memo, useEffect} from 'react';
import '../App.css';
import {
    Button,
    Card, CardContent,
    createStyles,
    FormControl, FormControlLabel, Radio, RadioGroup,
    TextField,
    withStyles,
    WithStyles
} from "@material-ui/core";
import {
    FinalEvaluationDTO,
    GrantApplicationDetailsDTO, GrantApplicationsApi,
    ReviewDTO
} from "../api";
import Appbar from "../Homepage/Appbar";
import {match, Redirect} from "react-router-dom";
import {getAuthorizationHeader} from "../DataManipulation";

const styles = createStyles({
    root: {
        display: 'flex',
        justifyContent:'center',
        alignItems:'center',
        maxWidth: 700,
        margin: '0 auto',
        float: 'none',
        marginTop: '50px',
        marginBottom: '50px',
        borderRadius: 10,
        position: 'relative',
    },
    button:{
        marginTop: '10px',
        marginRight: '150px',

    },
    passwordDiv:{
        marginTop: '20px',
    },
    title:{
        marginLeft: '10px'
    },
    textBox:{
        height:'auto',
        width: '450px',
        maxWidth: '450px',
        maxHeight: '300'
    },

    score:{
        width: '100px',
        maxWidth: '100px',
        marginTop: '20px'

    },
    submitReviewButton: {
        position: 'relative',
        marginTop: 30
    },
});

type TParams = { id: string };

type Props = {
    match: match<TParams>,
    isSignedIn: boolean,
    signIn:(b:boolean)=>void,
    isFinalEvaluation?: boolean,
    grantApplication: GrantApplicationDetailsDTO
};

export type CaptionedValueProps = Props & WithStyles<typeof styles>;



const EvaluationForm: React.FC<CaptionedValueProps> = (
    {classes, match, signIn, isSignedIn, isFinalEvaluation,
        grantApplication}) => {

    const [reviewEvaluation, setEvaluation] = React.useState('');
    const [reviewScore, setScore] = React.useState(0);
    const [finalStatus, setFinalStatus] = React.useState(false)
    const [redirectToProfile, setRedirectToProfile] = React.useState<boolean>(false);
    const [redirectToHome, setRedirectToHome] = React.useState<boolean>(false);
    const [existingApplication, setExistingApplication] = React.useState<GrantApplicationDetailsDTO>();

    useEffect(() => {
        const role: string | null = localStorage.getItem('role')

        if(role !== null && role === 'ROLE_REVIEWER') {
            if(grantApplication === undefined) {
                new GrantApplicationsApi().getApplication(+match.params.id, getAuthorizationHeader()).then(applications =>
                    setExistingApplication(applications)
                )
            }
            else {
                setExistingApplication(grantApplication)
            }
        }

        else
            setRedirectToHome(true)

    }, [match.params.id, grantApplication]);

    const onButtonClick = () => {

        const username = localStorage.getItem('username') || ""

        if(isFinalEvaluation && existingApplication) {
            const finalEvaluation: FinalEvaluationDTO = {
                accepted: finalStatus,
                evaluation: reviewEvaluation,
                reviewerUsername: username,
                score: reviewScore
            }

            new GrantApplicationsApi().submitFinalEvaluation(finalEvaluation, +match.params.id, getAuthorizationHeader())
                .then(response => {
                    if(response.ok)
                        setRedirectToProfile(true)
            })

        } else {
            const review: ReviewDTO = {
                evaluation: reviewEvaluation,
                reviewerUsername: username,
                score: reviewScore
            }

            new GrantApplicationsApi().submitReview(+match.params.id, review, getAuthorizationHeader())
                .then(response => {
                    if(response.ok)
                        setRedirectToProfile(true)
                })

        }

    }

    const handleChangeFinalStatus = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFinalStatus((event.target as HTMLInputElement).value == 'true');
    };

    const handleChangeTextfield = (text: string, isScore :boolean) => {
        if(isScore)
            setScore(+text)
        else
            setEvaluation(text);
    };

    return (
        <>
            {redirectToHome ? <Redirect to="/" /> :
                 redirectToProfile ? <Redirect to={{
                         pathname: `/applications/${match.params.id}`,
                         state: {
                             reviewed: true
                         }
                     }}/> :
                <>
                    <Appbar isSignedIn={isSignedIn} signIn={signIn}/>

                    <Card className={classes.root} elevation={3}>
                        <CardContent>
                            <div className={classes.title}>
                                <h2>Write a review</h2>
                            </div>
                            <div>
                                <TextField className={classes.textBox}
                                           id="outlined-multiline-static"
                                           label="Review"
                                           multiline
                                           rows={10}
                                           variant="outlined"
                                           onChange={(e) =>
                                               handleChangeTextfield(e.target.value, false)}
                                />


                            </div>
                            <div>
                                <h4> Enter the evaluation score </h4>
                                <TextField
                                    id="outlined-full-width"
                                    style={{ margin: 0 }}
                                    placeholder="Score"
                                    fullWidth
                                    type={'number'}
                                    margin="normal"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    variant="outlined"
                                    onChange={(e) =>
                                        handleChangeTextfield(e.target.value, true)}
                                />
                            </div>

                            {isFinalEvaluation &&
                                <div>
                                    <FormControl component="fieldset">
                                        <h4 style={{marginBottom: 5}}>Application's final status</h4>
                                        <RadioGroup aria-label="gender" name="gender1" value={finalStatus} onChange={handleChangeFinalStatus}>
                                            <FormControlLabel value={true} control={<Radio color={'primary'}/>} label="Accepted" />
                                            <FormControlLabel value={false} control={<Radio color={'primary'}/>} label="Denied" />
                                        </RadioGroup>
                                    </FormControl>
                                </div>
                            }

                            <Button className={classes.submitReviewButton} variant="contained" onClick={onButtonClick}>
                                {isFinalEvaluation ? "Submit Final Evaluation" : "Submit review"}
                            </Button>

                        </CardContent>
                    </Card>
                </>

            }

        </>

    );
}

const EvaluationFormWithStyles = memo(withStyles(styles)(EvaluationForm));
export default EvaluationFormWithStyles;
