import {createStyles, withStyles, WithStyles} from '@material-ui/core/styles';
import React, {memo, useEffect} from 'react';
import {
    AnswerFieldDTO,
    FinalEvaluationDTO,
    GrantApplicationDetailsDTO,
    GrantApplicationDTO, GrantApplicationsApi, GrantCallsApi,
    ReviewDTO
} from "../api";
import {
    Link,
    match,
    Redirect
} from "react-router-dom";
import {getAuthorizationHeader} from "../DataManipulation";
import Appbar from "../Homepage/Appbar";
import ApplicationQuestionListElement from "../GrantApplicationForm/ApplicationQuestionListElement";
import Typography from "@material-ui/core/Typography";
import {Button, Grid} from "@material-ui/core";
import ApplicationReviewListElement from "./ApplicationReviewsElement";
import StudentDetailsCard from "./StudentDetailsCard";
import SaveIcon from "@material-ui/icons/Save";

const styles = createStyles({
    root: {
        paddingBottom: 100
    },
    h4: {
        marginTop: 35,
        marginLeft: 35
    },
    accepted: {
        marginTop: 35,
        marginLeft: 35,
        color: 'green',
    },
    denied: {
        marginTop: 35,
        marginLeft: 35,
        color: 'red',
    },
    submitNowButton: {
        position: 'absolute',
        right: 30,
        marginTop: '20px'
    },
    saveNowButton: {
        position: 'absolute',
        right: 170,
        marginTop: '20px',
    },
    reviewButton: {
        position: 'absolute',
        right: 250,
        marginTop: '20px'
    },
    reviewButtonOnly: {
        position: 'absolute',
        right: 30,
        marginTop: '20px'
    },
});

type TParams = { id: string };

type Props = {
    isSignedIn: boolean,
    signIn:(b:boolean)=>void,
    match: match<TParams>,
    reviewed?: boolean,
    grantApplication?: GrantApplicationDetailsDTO
};

function fetchData(applicationDetailsDTO: GrantApplicationDetailsDTO, match: match<TParams>,
                   setReviewedApplication: (b: boolean) => void, reviewed: boolean|undefined, setReviews: (r: Array<ReviewDTO>) => void,
                   setFinalEvaluation: (f: FinalEvaluationDTO) => void, setIsPanelChair: (b: boolean) => void,
                   setAnswers: (a: Array<AnswerFieldDTO>) => void, setApplication: (a: GrantApplicationDetailsDTO) => void) {

    const answersDTO: AnswerFieldDTO[] = applicationDetailsDTO.answers.map(a => {
        return {
            dataItem: a.dataItem,
            questionID: a.question.id
        }
    });

    const role: string | null = localStorage.getItem('role')
    const username: string | null = localStorage.getItem('username')

    if((applicationDetailsDTO.status === 1 && role === 'ROLE_REVIEWER')
        || applicationDetailsDTO.status === 2) {

        new GrantApplicationsApi().getApplicationReviews(+match.params.id, getAuthorizationHeader())
            .then(reviews => {
                if(reviewed === undefined)
                    reviews.map(review => {
                        if(review.reviewerUsername === username) {
                            setReviewedApplication(true)
                        }
                    })
                else
                    setReviewedApplication(reviewed)

                setReviews(reviews)
            })
    }

    if(applicationDetailsDTO.status === 2) {
        new GrantApplicationsApi().getFinalEvaluation(+match.params.id, getAuthorizationHeader())
            .then(finalEvaluation => setFinalEvaluation(finalEvaluation))
    }

    if(role === 'ROLE_REVIEWER') {
        new GrantCallsApi().getEvaluationPanel(applicationDetailsDTO.grantCallId, getAuthorizationHeader())
            .then(evalPanel => {
                if(evalPanel.panelChair.username === username)
                    setIsPanelChair(true)
            })
    }

    setAnswers(answersDTO)
    setApplication(applicationDetailsDTO)
}

export type CaptionedValueProps = Props & WithStyles<typeof styles>;

const ApplicationDetails: React.FC<CaptionedValueProps> =
    ({classes, isSignedIn, signIn, match, reviewed,
         grantApplication}) => {

    const [application, setApplication] = React.useState<GrantApplicationDetailsDTO>();
    const [redirectToProfile, setRedirectToProfile] = React.useState<boolean>(false);
    const [redirectToHome, setRedirectToHome] = React.useState<boolean>(false);
    const [answers, setAnswers] = React.useState<Array<AnswerFieldDTO>>([] as Array<AnswerFieldDTO>);
    const [reviews, setReviews] = React.useState<Array<ReviewDTO>>([] as Array<ReviewDTO>);
    const [finalEvaluation, setFinalEvaluation] = React.useState<FinalEvaluationDTO>();
    const [isPanelChair, setIsPanelChair] = React.useState<boolean>(false);
    const [reviewedApplication, setReviewedApplication] = React.useState<boolean>(false);

    useEffect(() => {
        if(grantApplication !== undefined) {
            fetchData(grantApplication, match,
                setReviewedApplication, reviewed, setReviews,
                setFinalEvaluation, setIsPanelChair,
                setAnswers, setApplication)

            setApplication(grantApplication)
        }
        else {
            new GrantApplicationsApi().getApplication(+match.params.id, getAuthorizationHeader())
                .then(application => {
                    fetchData(application, match,
                        setReviewedApplication, reviewed, setReviews,
                        setFinalEvaluation, setIsPanelChair,
                        setAnswers, setApplication)
                })
                .catch(() => setRedirectToHome(true))
        }


    }, [match.params.id]);


    const role: string | null = localStorage.getItem('role')

    const onButtonClick = (callStatus: number) => {
        if(application) {

            let mandatoryEmpty: boolean = false;
            let a = answers.slice()
            application.answers.map((answer, index) => {

                const checkEmptyAnswer = !answers[index] ||
                    (answers[index] && (answers[index].dataItem == undefined || answers[index].dataItem === ""))

                if(checkEmptyAnswer) {
                    a[index] = {
                        dataItem: '',
                        questionID: answer.question.id
                    }
                }

                if(answer.question.mandatory && checkEmptyAnswer)
                    mandatoryEmpty = true;

            })

            setAnswers(a)

            if(mandatoryEmpty) {
                alert("Please answer all mandatory questions")
            }
            else {

                const date: Date = new Date()
                const newApplication: GrantApplicationDTO = {
                    submissionDate: date,
                    status: callStatus,
                    answers: answers,
                    grantCallId: application.grantCallId,
                    grantCallTitle: application.grantCallTitle
                }

                new GrantApplicationsApi().updateApplication(newApplication, application.id, getAuthorizationHeader())
                    .then(response => response.ok && setRedirectToProfile(true))

            }
        }

    }

    const buildAnswerArray = (childIndex: number, answerDTO: AnswerFieldDTO) => {
        let a = answers.slice()
        a[childIndex] = answerDTO

        setAnswers(a)
    }

    const reviewsAndFinalEvaluationSection =
        <>
            { application &&

            <>
                {reviews && reviews.length > 0 &&
                    <>
                        <Typography variant="h5" component="h2" className={classes.h4}>
                            Reviews
                        </Typography>
                        {reviews.map((review, index) =>
                            <ApplicationReviewListElement key={index} review={review}/>)}
                    </>
                }

                { finalEvaluation &&
                    <>
                        <Grid container>
                            <Typography variant="h5" component="h2" className={classes.h4} align={"left"}>
                                Final Evaluation: {finalEvaluation.score}
                            </Typography>
                            {finalEvaluation.accepted ?
                                <Typography variant="h5" component="h2" className={classes.accepted} align={"right"}>
                                    Accepted
                                </Typography>
                                :
                                <Typography variant="h5" component="h2" className={classes.denied} align={"right"}>
                                    Denied
                                </Typography>
                            }

                        </Grid>
                        <ApplicationReviewListElement review={finalEvaluation}/>
                    </>
                }

            </>}
        </>

    const studentPerspective =
        <>
            {redirectToProfile ? <Redirect to="/profile"/> :
                <>
                    <Typography variant="h5" component="h2" className={classes.h4}>
                        Answers to Grant Call: {application?.grantCallTitle}
                    </Typography>

                    {application && application.answers.map((answer, index) =>
                        <ApplicationQuestionListElement
                            key={index} question={answer.question} index={index}
                            existingAnswer={answer.dataItem}
                            buildAnswerArray={buildAnswerArray}
                            submitted={application?.status >= 1}
                        />
                    )}

                    {application?.status === 0 &&
                        <>
                            <Button className={classes.saveNowButton} variant="contained"
                                    onClick={() => onButtonClick(0)} startIcon={<SaveIcon />}>
                                Save
                            </Button>
                            <Button className={classes.submitNowButton} variant="contained"
                                    onClick={() => onButtonClick(1)}>
                                Submit now
                            </Button>
                        </>
                    }

                    {reviewsAndFinalEvaluationSection}

                </>
            }
        </>
    
    const reviewerPerspective =
        <>
            <Typography variant="h5" component="h2" className={classes.h4}>
                Submission details
            </Typography>

            {application && application.answers.map((answer, index) =>
                <ApplicationQuestionListElement
                    key={index} question={answer.question} index={index}
                    existingAnswer={answer.dataItem}
                    buildAnswerArray={buildAnswerArray}
                    submitted={application?.status >= 1}
                />
            )}

            {reviewsAndFinalEvaluationSection}

            { isPanelChair && application && application.status < 2 &&
                <Link to={{
                    pathname: `/applications/${application?.id}/finalEvaluation`,
                    state: {
                        grantApplication: application
                    }
                }}>
                    <Button className={classes.submitNowButton} variant="contained">
                        Write Final Evaluation
                    </Button>
                </Link>
            }

            { application && application.status < 2 && !reviewedApplication &&
                <Link to={{
                    pathname: `/applications/${application?.id}/review`,
                    state: {
                        grantApplication: application
                    }
                }}>
                    <Button className={isPanelChair ? classes.reviewButton : classes.reviewButtonOnly}
                            variant="contained">
                        Write Review
                    </Button>
                </Link>
            }

        </>



    const pageByPerspective = role == 'ROLE_STUDENT' ? studentPerspective : reviewerPerspective


    return (<>
        {redirectToHome ? <Redirect to="/" /> :
            <div className={classes.root}>
                <Appbar isSignedIn={isSignedIn} signIn={signIn}/>

                {application &&
                    <>
                        <Typography variant="h5" component="h2" className={classes.h4}>
                            Student details
                        </Typography>

                        <StudentDetailsCard student={application.studentDetails} />
                    </>
                }

                {pageByPerspective}

            </div>
        }

    </>);

}


const ApplicationDetailsWithStyles = memo(withStyles(styles)(ApplicationDetails));
export default ApplicationDetailsWithStyles;