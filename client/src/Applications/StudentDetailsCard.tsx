import React from 'react';
import {createStyles, makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {StudentDetailsDTO} from "../api";


const Styles = makeStyles(() =>
    createStyles({

        cardContent: {
            marginLeft: 30,
            marginRight: 30,
            marginTop: 20,
            marginBottom: 20,
            minWidth: 275,
            borderRadius: 10,
        },
    }),
);


export default function StudentInformationCard ({student}: {student: StudentDetailsDTO}) {

    const classes = Styles();

    return (
        <Card elevation={3} className={classes.cardContent} color={"primary"}>
            <CardContent>
                <Typography variant="body2" component="div" >
                    <ul style={{margin:0}}>
                        <li> <b> Student name: </b> {student.name} </li>
                        <li> <b> Student number: </b> {student.number} </li>
                        <li> <b> Student email: </b> {student.email} </li>
                        <li> <b> Student address: </b> {student.address} </li>
                        <li> <b> Student course: </b> {student.course} </li>
                        <li> <b> Student institution: </b> {student.institution.name} </li>
                        <li> <b> Student phone number: </b> {student.phoneNumber} </li>

                    </ul>
                </Typography>
            </CardContent>
        </Card>
    );
}

