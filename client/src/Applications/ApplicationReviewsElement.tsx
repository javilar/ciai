import React from 'react';
import {createStyles, makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {FinalEvaluationDTO, ReviewDTO} from "../api";


const Styles = makeStyles(() =>
    createStyles({

        cardContent: {
            marginLeft: 30,
            marginRight: 30,
            marginTop: 20,
            marginBottom: 20,
            minWidth: 275,
            borderRadius: 10,
        },
    }),
);

export default function ApplicationReviewListElement ({review}: {review: ReviewDTO|FinalEvaluationDTO}) {


    const classes = Styles();

    return (
        <Card elevation={3} className={classes.cardContent} color={"primary"}>
            <CardContent>
                <Typography variant="body2" component="div" >
                    <ul style={{margin:0}}>
                        <li> <b> Reviewer: </b> {review.reviewerUsername} </li>
                        <li> <b> Evaluation: </b> {review.evaluation} </li>
                        <li> <b> Score: </b> {review.score} </li>
                    </ul>
                </Typography>
            </CardContent>
        </Card>
    );
}

