import {createStyles, withStyles, WithStyles} from '@material-ui/core/styles';
import React, {memo, useEffect} from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {GrantApplicationDetailsDTO, ReviewersApi, StudentsApi} from "../api";
import Typography from "@material-ui/core/Typography";
import Appbar from "../Homepage/Appbar";
import GrantApplicationListElement from "./GrantApplicationListElement";
import {Redirect} from "react-router-dom";
import {getAuthorizationHeader} from "../DataManipulation";

const styles = createStyles({
    root: {
        marginBottom: '30px'
    },
    h4: {
        marginTop: 35,
        marginLeft: 35
    },
    formControl: {
        margin: '10px',
        minWidth: 150,
        marginLeft: 'auto',
        marginRight: 35
    },
    rowC: {
        marginTop: 10,
        display:'flex',
        flexDirection:'row',
    },
    pos: {
        marginTop: 12,
        marginBottom: 30,
        marginLeft: 35
    },
    emptyList: {
        position: 'absolute',
        left: '50%',
        marginTop: 40,
        transform: 'translate(-50%, -50%)'
    }
});

const optionsByRole = (role: string|undefined): JSX.Element => {
    switch (role) {
        case "ROLE_STUDENT":
            return <>
                <option value={0}>All</option>
                <option value={1}>Created</option>
                <option value={2}>Submitted</option>
                <option value={3}>Evaluated</option>
            </>
        case "ROLE_REVIEWER":
            return  <>
                <option value={3}>Panels I belong</option>
                <option value={4}>Panels I lead</option>
                </>
    }

    return <></>
}

type Props = {
    isSignedIn: boolean,
    signIn:(b:boolean)=>void
};

export type CaptionedValueProps = Props & WithStyles<typeof styles>;

const ProfilePage: React.FC<CaptionedValueProps> = ({classes, isSignedIn, signIn}) => {

    const [filter, setFilter] = React.useState(-1);
    const [userRole, setUserRole] = React.useState<string>();
    const [applications, setApplications] = React.useState<Array<GrantApplicationDetailsDTO>>([]);
    const [filteredList, setFilteredList] = React.useState<Array<GrantApplicationDetailsDTO>>([]);

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        const value: number = event.target.value as number

        if(userRole === 'ROLE_STUDENT') {
            const list = value == 1 ? applications.filter(application => application.status === 0)
                : value == 2 ? applications.filter(application => application.status === 1)
                : value == 3 ? applications.filter(application => application.status === 2)
                    : applications

            setFilteredList(list)
        }
        else if(userRole === 'ROLE_REVIEWER') {
            const username: string | null = localStorage.getItem('username')

            if(username != null && value == 3) {
                new ReviewersApi().getReviewerApplicationsUsingGET(username, getAuthorizationHeader())
                    .then(applications => {
                        setFilteredList(applications)
                })
            }
            else if(username != null && value == 4) {
                new ReviewersApi().getPanelChairApplicationsUsingGET(username, getAuthorizationHeader())
                    .then(applications => {
                        setFilteredList(applications)
                })
            }
        }

        setFilter(event.target.value as number);
    };

    useEffect(() => {

        const role: string | null = localStorage.getItem('role')
        const username: string | null = localStorage.getItem('username')
        const jwtToken: string | null = localStorage.getItem('jwt')

        if(role != null && username != null && jwtToken != null) {

            if(role === 'ROLE_STUDENT') {
                new StudentsApi().getStudentApplicationsUsingGET(username, getAuthorizationHeader())
                    .then(applications => {
                        setApplications(applications)
                        setFilteredList(applications)
                })
            }
            else if(role === 'ROLE_REVIEWER') {
                new ReviewersApi().getReviewerApplicationsUsingGET(username, getAuthorizationHeader())
                    .then(applications => {
                        setApplications(applications)
                        setFilteredList(applications)
                })
            }

            setUserRole(role)
        }

    }, []);


    const name = localStorage.getItem('name');

    const list = filteredList.length > 0 ? filteredList.map((application, index) =>
            <GrantApplicationListElement key={index} application={application}/>
        ) : <Typography className={classes.emptyList} color="textSecondary">No applications for the specified filter</Typography>

    return (
        <>
            {name === null ? <Redirect to="/"/> :
                <div className={classes.root}>
                    <Appbar isSignedIn={isSignedIn} signIn={signIn}/>
                    <div className={classes.rowC}>
                        <Typography variant="h5" component="h2" className={classes.h4}>
                            {name}
                        </Typography>

                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel htmlFor="outlined-age-native-simple">Filter</InputLabel>
                            <Select
                                native
                                value={filter}
                                onChange={handleChange}
                                label="Filter"
                                inputProps={{
                                    name: 'filter',
                                    id: 'outlined-age-native-simple',
                                }}
                            >
                                {optionsByRole(userRole)}
                            </Select>
                        </FormControl>

                    </div>

                    <Typography className={classes.pos} color="textSecondary">
                        Grant Applications
                    </Typography>

                    {list}
                </div>
            }

        </>

    );
}

const ProfilePageWithStyles = memo(withStyles(styles)(ProfilePage));
export default ProfilePageWithStyles;