import React from 'react';
import {createStyles, makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {GrantApplicationDetailsDTO} from "../api";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            marginLeft: 70,
            marginRight: 70,
            marginTop: 20,
            minWidth: 275,
            borderRadius: 10
        },
        rowC: {
            display:'flex',
            flexDirection:'row',
        },
        status: {
            marginLeft: 'auto',
            marginRight: 35,
            marginTop: '25px'
        },
        typographyText:{
            '&:focus, &:hover, &:visited, &:link, &:active': {
                textDecoration: 'none',
            }
        },
        pos: {
            marginTop: 0,
            marginBottom: 8,
        },
        created: {
            color: 'gray',
        },
        submitted: {
            color: 'darkgray',
        },
        evaluated: {
            color: 'green',
        },
    }),
);

export function applicationColorAndText(classes: Record<any, any>, status: number|undefined): [string, string] {

    switch(status) {
        case 0:
            return [classes.created, "Created"]
        case 1:
            return [classes.submitted, "Submitted"]
        case 2:
            return [classes.evaluated, "Evaluated"]
    }

    return [classes.created, ""];
}

export default function CallListElement({application}:{application:GrantApplicationDetailsDTO}) {
    const classes = useStyles();

    const [ statusColor, statusText ] = applicationColorAndText(classes, application.status)

    const role = localStorage.getItem('role') || ""

    return (
        <Card elevation={3} className={classes.root}>
            <CardContent>
                <div className={classes.rowC}>
                    <div style={{marginTop: '10px'}}>

                        <Link className={classes.typographyText} to={{
                            pathname: `/applications/${application.id}`,
                            state: {
                                grantApplication: application
                            }
                        }}>
                            <Typography variant="h6">
                                {application.grantCallTitle}
                            </Typography>
                        </Link>

                        {role === 'ROLE_REVIEWER' &&
                            <Typography className={classes.pos} color="textSecondary">
                                Student: {application.studentDetails.name}, {application.studentDetails.number}
                            </Typography>
                        }

                        {role === 'ROLE_STUDENT' &&
                            <Typography className={classes.pos} color="textSecondary">
                                {application.status === 0 ? "Created in:" : "Submitted in:"} {application.submissionDate}
                            </Typography>
                        }

                    </div>

                    <h2 className={`${classes.status} ${statusColor}`}>{statusText}</h2>

                </div>
            </CardContent>
        </Card>
    );
}

